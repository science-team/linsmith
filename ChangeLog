2015/11/26 v0.99.31
        - Apparently, at some point - a long time ago - I removed a
          lot of code for the remote control from linsmith and forgot
          completely about it. I gather this is not a piece of code 
          frequently used, but at least I should have put in some
          advisory! My apologies for this!
        - I added completely new code for the in/out pipes, which 
          also is somewhat more modern, and should be more robust.
2015/11/24 v0.99.30
        - The serial port wasn't saved correctly in configuration.
2014/06/20 v0.99.28
	- New compiler security options, which are apparently in use
	  by some packagers, threw a couple of warnings. Indirectly
	  they indicated another potential bug. All of these are now
	  corrected. Thanks to Graham Inggs for sending me the 
	  compile logs.
	- NOTE FOR PACKAGERS: Don't understand me wrong, I _really_ 
	  appreciate the work you're doing! I would appreciate it if
	  you could warn the program author of any wrongdoings. I have
	  no way (except bug reports from third parties) to know if
	  my program is your catalog or not!
2014/06/17 v0.99.27
	- Debian reported a warning about a missing parameter in
	  misc.c call to recalculate_all. For some reason gcc didn't
	  report any warnings for me. But it _is_ an error. Corrected.
	- Newer versions of gtk+ cause a warning about a deprecated
	  _unref call. Though this doesn't cause any problem, I've
	  updated the call to the new g_object format.
2013/07/18 v0.99.26
	- A bug in confmgr.c caused errors when reading strings from
	the configuration (thanks Graham Inggs)
2013/01/09 v0.99.25
	- A rather important bug infiltrated when changeing the
	configuration system. Though I carefully prepared the
	default values for the vector map, I entered them in hex
	which them caused a white chart.
	If you had/have this problem, the easiest way out is to
	use this new version, but first deleting the previous
	configuration file (in your home directory .gnome2/linsmith)
	- To encourage the use of the vector chart anyway, it is
	now the default for new installs.
2012/12/04 v0.99.24
	- Print bug correct (Segfault on second call to print).
	(Thanks for reporting, Clyde!)
	- Cleanups in the configuration load/save functions
2011/06/22 v0.99.23
	- Made some changes in automake.am and configure.ac to
	avoid confusion if importing the project into Anjuta.
	- Split cable table routines from misc.c into
	a separate module.
	- Added configuration of line width (Z, Y, Line, Z/Y) for
	on-screen display
2011/03/21 v0.99.22
        - Change papersize_combo to combobox, and load the list from 
        the available papersizes.
        - Adapted confmgr to accept char * from comboboxes.
        - Moving down elements caused a segfault, because of wrongly 
        g_free'ing an iter. (Thanks clydes!)
        - Moving elements up implemented.
        - A crash in the configuration dialog fixed, which made it 
        impossible to save the SWR circle color. (Thanks clydes!)
        - Converted the strange negative default values for the color 
        definitions to their (easier to understand?) hex values.
        - The 'standard cable' code mostly re-written, and a few
        more cable types added.
2011/03/20 v0.99.21
        - Removed the (deprecated) gnomeprint depency. 
        Now the complete print route relies on Pango/Cairo to 
        generate the plot, and the GtkPrint interface to do the actual 
        printing. (After some poking from Thomas Beierlein, from gentoo.
        Thanks)
2010/05/14 v0.99.20
        - Slight improvements to the vector plot.
        - Better (?) default values for colors of plot.
        - Modified the confmgr to accept hex and octal values in the 
        configuration file.
2009/05/08 v0.99.12
	- Missing NULL in run_filedialog caused warnings when saving 
	the log. In some cases it could cause segmentation faults too.
	Thanks to the guys at Fedora for notifying me!
2008/09/02 v0.99.11
	- Swedish translation updated (Thanks Peter!)
	- Smaller bugs corrected which could cause a few problems if 
	z0 was changed. (chart.c - Thanks for the indications, Anatoly!)
2008/08/22 v0.99.10
	- The last s2p file's name is now saved in the configuration. 
	Had forgotten that in the previous version.
2008/08/21 v0.99.9
	- Added import of Touchstone(r) two-port files. Until now, only
	Version 1.0 files are correctly parsed, though completely (I 
	hope) with all parameter types, and input formats.
	- Spanish translation has been updated.
	- Manual has been updated with this info.
2008/05/26 v0.99.7
	- Dustin Vaselaar reports problems inputing loads. 'errno' is 
	now cleared before each strtod call.
2008/04/25 v0.99.6
	- Applied a patch to fix possible startup crash in case of 
	missing background charts (provided by bernardb - thanks!)
	- Added a more complete syntax check on load input. Empty 
	fields are not permitted anymore. Check is done on both the 
	normal and noisebridge input pages.
	- Typo in preferences ('connectios')
	- Added import of impedances in CSV format
	- Added dialog to warn about existing loads when importing or 
	loading new values.
2007/10/11 v0.99.5
	- Changed the name of aux.c and aux.h to misc.c and
	misc.h, because when I tried to compile linSmith
	on Windoze, I found out that aux (no matter what the
	extension) is reserved by the operating system 
	(DOS inheritance).
	- Changed the open/close file dialogs to the
	newer, and much improved filechooserdialog.
	- Experimentally added import for TouchstoneTM files
	(s2p format, incomplete)).
	- Moved calls to the open/save functions out of
	callbacks.c and to the corresponding files.
2007/01/03 v0.99.4
        - Hans Nieuwenhuis reported an error in log.c
	which could cause crashes when generating a data
	log. Thanks!                                    
2006/11/15 v0.99.3
	- A few other potential char array traps removed.
	These are of less importance as the reserved space
	was more than enough to accomodate the strings.
2006/11/06 v0.99.2
	- Thanks to Alexander to report a craching problem,
	and to the people at Ubuntu (Bernard) to pointing
	out the problem. Long font names caused this.
	I've replaced these (static) buffers by dynamically
	assigned ones.

	Please, distro people, I'd appreciate it if you
	advise me when you include linSmith in your package!
	I can provide a link to your package, and I can
	be prepared for specific problems!

2006/07/09 v0.99.1
	- Added the vector-generated background map for
	the chart.
	- Modified configuration to allow for definition
	of the background color of the vector chart.
	- Corrected the font selection for the vector
	chart (use GtkFontButton).
	- Added a config parameter to set the initial size
	of the vector chart.
	- Optimized the chart generation by eliminating 
	symmetric reactance circles from the table.
	- Modified the table into a proper .h and .c file.
	- Added zoom factors
2006/06/24 v0.9.8
	- Version in component files is now the complete
	version in a single string. This should not
	raise any incompatibilities.
	- Updated es.po
	- Modified the Results page: Circuit impedance
	is now shown as either Z or Y depending on the
	Z/Y blocks inserted in the element list. This
	can be disabled (forced to impedance only) in
	Preferences. (This also applies to Results
	export file)
	- Separate resolutions settable for impedances
	and admittances.
2006/06/19 v0.9.7
	- Corrected the 'live' display of the cursor
	values under the graph - they remained at
	50 Ohm even after changing z0 (Thanks Anatoly!
	tarant941[at]m-lan.ru)
2006/04/10 v0.9.6
	- Corrected the connecting arcs for 'to load' rotation.
	- Modified the load impedance input to select
	different input data sources and types.
	- Don't enable the 'New' or 'Update' buttons if 
	the values are not valid (f != 0).
	- Noise bridge input works! Input in R and C,
	values are shown as R and X in the load table.
2006/04/06 v0.9.5
	- Added a 'Clear' button to the load and element lists.
	- Replaced large 'Recalculate' button by a smaller
	version, and moved it to the left, to make space
	for the rotation list.
	- Added the Rotate list, to select rotation to
	generator or to load. Added the option to the
	preferences record. Note that though transmission
	lines already rotate correctly, the connecting
	line is still wrong.
	- Setting the Zo box on startup caused errors
	in the (not yet existing) log_list. Solved.
	- Minor bugfix in configuration window. Screen
	font name was not correctly saved and could cause
	execution problems.
	- After configuration, chart must be in non-logo
	mode, else an error message is produced when
	trying to update the SWR circle
2006/03/30 v0.9.4
	- Chart also updates when loads are added or modified.
	- Infrequent bug detected in load management. Could
	be the problem while importing from remote?
	- Redefined (improved) quite a few routines in 
	the load.c file. This caused a few bugs (which I
	hope, are resolved). Please report on any problems.
2006/03/22 v0.9.3
	- Changed order of element definition: Select
	Series/parallel first, the actual component. Show
	only the possible components in each case. This
	causes quite a few changes in aux.c.
	- Error in setting the component buttons solved.
	- Changing Zo on the main window now has immediate effect.
	- (More or less) centralized the enabling the buttons on
	the element page. Hope this improves the usability.
2005/11/03 v0.9.2
        - IMPORTANT: It seems there were some problems with the 
        connecting impedance/admittance circles	in the previous
        version. I hope this is now resolved!
	- Added another example and revised the data files.
	- Margarita <debian[at]marga.com.ar> did some
	exceptional work, correcting the es-translation,
	adapting some makefiles, she even provided an
	icon and a man-page for linSmith! Thanks...
	- I've removed the doc-generation files entirely,
	only the final PDF is included. There were reports
	about low-quality fonts, and other issues which
	are not easily addressable in the automatic install
	procedure.
	- Corrected the 'Save results...' option
	permitting saving the numerical results to a file.
2005/10/17 v0.9.1
        - Added filenames to the 'loads' and 'circuit'
	page, and an 'changed' flag to indicate the list
	has been modified.
	- Added 'Save results page' option in the 'Files'
	menu, to save the numerical results
	- Added a Swedish translation (Thanks to
	Peter Landgren peter.talken[at]telia.com!)
2005/05/15 v0.9.0a3
	- An error in calculating parallel LC circuits
	sneaked into the code cleanup (corollary: never
	clean up code?)
	- The number of decimals in frequency is now
	settable in the configuration.
	- SWR circle immediately redraw after saving config.
	- Expanded the cable table, changed selection
	- Updated es.po, and partially de.po. My german
	is not too good I'm afraid.
2005/05/12 v0.9.0a2
	- Value text for Z/Y component wasn't initialized.
	- On Lawrence's request, I changed the frequency
	resolution to 3 decimals
	- Update and New buttons were not activated
	reliably - solved
	- Main screen component editor showed uH, should
	be nH - corrected (again Lawrence...)
2005/04/02 v0.9.0a1
	- Added parallel and series LC combination which
	were difficult to enter before. This caused a huge
	number of other changes - even in the circuit file
	data format. This new format isn't compatible!
	I wrote a small script in Tcl which can convert
	the old format (conv0809).
	- Changed the layout of the component input page
	- Changed the layout of the load input page
	- Much needed cleanup of callbacks.c
	- Changed the component scrollbars. They will now
	auto-return to center, if not held with the mouse,
	after 1 second. This makes it much easier and faster
	to experiment.
	- Modified the format of the component list to
	accomodate par/ser LC combinations.
	- I'm using the Omega sign to format the line info
	This could cause problems with some character sets
	if the symbol isn't defined. Please report if this
	doesn't work for you.
	- Save file paths at any save or load
	- Solved a bug which didn't show connections
	with stubs
	- Improved (more logical) component description
	- Solved a bug with 'updating' open/closed stubs
	- Found and solved a bug in saving transmission
	line components.
	- Updated predefined cable selection to the new
	system.
2004/12/23 v0.8.4
	- Fixed a (double) bug in the zoom procedure,
	particularly visible when zooming out (thanks to
	Lawrence Glaister ve7it[at]shaw.ca for reporting
	this).
	- The configuration interface is present to
	set up the remote control. It is _not_ enabled yet!
2004/12/09 v0.8.3
	- Added a German translation (Thanks to Georg Baum,
	Georg.Baum[at]post.rwth-aachen.de)
	- Please check the NEWS file!
2004/09/10 v0.8.2
	- Added the TODO list to the distro... Maybe someone
	helps out?
2004/08/17 v0.8.1
	- Added #include of getopt.h to main.c. Though not
	necessary on my machine, some configurations do need
	it, and, really, it should be there. (Thanks
	Sid Boyce, G3VBV, sboyce[at]blueyonder.co.uk)
	- One of the 'todo-list' items implemented: the
	effect of changing the value scrollbars is now
	immediately visible on the chart. This is really
	quite spectacular. (Thanks Lawrence Glaister VE7IT,
	for reminding me)
2004/08/03 v0.8.0
	- Adapted the config files and the config screen
	to be able to select properties for the dynamic
	on-screen background. (Though the dynamic chart is
	not enabled yet)
	- Enabled the zoom buttons (can now zoom from 0.1
	to 10x)
2004/07/24 v0.7.6
	- Added Chinese translation (Thanks to Once,
	once_shuiqing[at]hit.edu.cn)
	- Added Spanish translation
	- Once also reported a bug when clicking 'Modify'
	on the loads page (corrected)
2004/07/18 v0.7.5
	- En error in the configuration files causes
	compilation errors on some systems. This should fix
	it. Please update.
2004/07/17 v0.7.4
	- More suggestions from Georg (thanks!):
	- Size of up/down buttons in Circuit (done)
	- Size of ok/cancel buttons on config dialog (done)
	- Moved 'print'-checkbutton to a File-menu item
	- Confirmation before overwriting previous export
	- Font picker added to select. The font selection
	  mechanism is too picky (sic) to just use a entry
	  widget.
2004/07/15 v0.7.3
	- Thanks to Georg Baum for the testing & suggestions.
	- Change the initial (logo) pixmap path in chart.c
	to include PACKAGE_DATA_DIR
	- Change the default pixmap path in global.c
	to include PACKAGE_DATA_DIR
	- Set the correct scale/offsets for the chart included
	- Included a few examples to be installed in
	<prefix>/linsmith
	- <shame> Now actually included the logo pixmap
2004/06/26 v0.7.2
	- Added SWR circle and G=1 circle, and the configs
	to match. Checked both on-screen and PS output.
	- Added connecting linse for Z/Y switches. Tested
	on most combinations.
	- Enabled the transformer (no connecting lines...
	are they advisable?)
	- Changed the screen bg chart properties to double
	(from int), as sometimes fractional corrections seem
	to be appropiate.
2004/06/22 v0.7.1
	- Added the Z/Y element to switch between impedances
	and admittances. Modified many functions to adjust
	for this trick.
	- Apologized to the gnome-print group for the 
	accusation... The fault was mine.
2004/06/10 v0.6.1
	- Got the PS output working. Mind - there is still a
	problem with gnomeprint - the first string printed
	with a global scaling active is printed in the
	wrong size. (Added a dummy label to prevent this)
2004/05/22 v0.3.1
	- Start keeping a changelog
	- Call calc_el_impedance only when needed, i.e. not 
	when adding a series line.
