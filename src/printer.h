/*
 *  printer.c: Postscript generator - high level routines calling gnome-print
 *
 *  Copyright (C) 1997- John Coppens
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <gtk/gtk.h>

enum {  DST_SCREEN,
        DST_PRINT
};

void            print_chart_start(void);
void		print_chart(char *fn);
void		print_do_arc(double x, double y,
		     double arcb, double arce, double rad);
void    	print_do_line(double x1, double y1, double x2, double y2);
void		print_do_string(char *str, double x, double y,
        		        double size, double angle);
void		print_point(double x, double y, int pstyle);

void		print_load_font_list(GtkCombo *combo);
void		print_setlinestyle(double lw, int color);
gboolean	print_setfontstyle(char *font, double size, int color);

