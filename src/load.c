/*
 *  load.c: Load related functions, list management etc.
 *
 *  Copyright (C) 1997-2005 John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <libxml/tree.h>
#include <gnome.h>
#include <unistd.h>

#include "support.h"
#include "callbacks.h"
#include "global.h"
#include "main.h"
#include "load_nb.h"
#include "load_rx.h"
#include "load.h"
#include "chart.h"
#include "misc.h"

typedef struct {
  char *hdr;
  int w;
} loaddef_entry;

loaddef_entry loaddef[] = {
  {N_("F (MHz)"), 80}, 
  {N_("Real"),    60}, 
  {N_("Imag"),    50}
};
#define ldef_len sizeof(loaddef)/sizeof(loaddef_entry)

GtkTreeIter	active_ld_iter;
load_definition	active_load;
GtkWidget	*ld_view;
int		load_mode = LOADMODE_R_JX;

//---------------------------------------------------------------------
//	Loads
//---------------------------------------------------------------------

void
load4_activate(void)
{
  if (number_loads() != 0) {
    switch (list_not_empty_dialog(
	_("The load list is not empty. Selecting Append will append\n"
	  "the new values."))) {
      case LSTDLG_APPEND: break;
      case LSTDLG_CLEAR : loadlist_clear(); break;
      case LSTDLG_CANCEL: return;
    }
  }

  if (run_filedialog(_("Load load impedances"), pref.last_ld_file, TRUE,
		_("linSmith loads"), "*.load", 
		_("All files"), "*",
		NULL)){
    load_loads(pref.last_ld_file);
    save_file_paths();
  }
}

void
save5_activate(void)
{
  if (strlen(pref.last_ld_file) != 0)
    save_loads(pref.last_ld_file);   
  else
    save_as3_activate();
}

void
save_as3_activate(void)
{
  if (run_filedialog(_("Save load impedances as"), pref.last_ld_file, FALSE, 
		_("linSmith loads"), "*.load", 
		_("All files"), "*",
		NULL)) {
    save_loads(pref.last_ld_file);
    save_file_paths();
  }
}


gint
compare_frequencies(GtkTreeModel *model, GtkTreeIter *a, GtkTreeIter *b,
			gpointer user_data)
{
  double f1, f2;

  gtk_tree_model_get(model, a, LD_FREQ_VAL, &f1, -1);
  gtk_tree_model_get(model, b, LD_FREQ_VAL, &f2, -1);

  if (f1 < f2)
    return -1;
  else
    if (f1 == f2)
      return 0;
    else
      return 1;
}


void
initialize_loadtable(void)
{
  GtkListStore *loads;
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *col, *sortcol;
  int def;
  
  loads = gtk_list_store_new(LD_COLS, 
		G_TYPE_STRING,		// Frequency (on-screen)
		G_TYPE_STRING,		// Resistance (on-screen)
		G_TYPE_STRING,		// Reactance (on-scren)
		G_TYPE_DOUBLE,		// load f
		G_TYPE_DOUBLE,		// load r
		G_TYPE_DOUBLE,		// load x
                G_TYPE_DOUBLE,          // nb r
                G_TYPE_DOUBLE,          // nb c
                G_TYPE_CHAR,            // nb extender
		G_TYPE_POINTER,		// loadpoint canvas item
		G_TYPE_POINTER);	// result column
  
  ld_view = lookup_widget(MainWindow, "load_treeview");
  gtk_tree_view_set_model(GTK_TREE_VIEW(ld_view), GTK_TREE_MODEL(loads));
  g_object_unref(G_OBJECT(loads));

  for (def = 0; def < ldef_len; def++) {
    renderer = gtk_cell_renderer_text_new();
    gtk_object_set(GTK_OBJECT(renderer), 
		"height", 14, "ypad", 0, 
    		"xalign", 1.0, NULL);
    col = gtk_tree_view_column_new_with_attributes(
		_(loaddef[def].hdr),
		renderer, "text", def,
		NULL);
    if (def == 0) sortcol = col;
    gtk_tree_view_append_column(GTK_TREE_VIEW(ld_view), col);
    gtk_tree_view_column_set_sizing(GTK_TREE_VIEW_COLUMN(col),
                GTK_TREE_VIEW_COLUMN_FIXED);
    gtk_tree_view_column_set_fixed_width(GTK_TREE_VIEW_COLUMN(col),
                loaddef[def].w);
  }

  gtk_tree_sortable_set_sort_func(GTK_TREE_SORTABLE(loads), LD_FREQ_VAL,
		compare_frequencies, NULL, NULL);
  gtk_tree_view_column_set_sort_column_id(sortcol, LD_FREQ_VAL);
}


int
number_loads(void)
{
  GtkTreeModel *model = gtk_tree_view_get_model(GTK_TREE_VIEW(ld_view));

  return gtk_tree_model_iter_n_children(model, NULL);
}


int
number_selected_loads(void)
{
  GtkTreeSelection *sel;

  sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(ld_view));
  return gtk_tree_selection_count_selected_rows(sel);
}


void
switch_load_mode(int newpage)
{
  switch (newpage) {
    case 0: load_mode = LOADMODE_R_JX; break;
    case 1: load_mode = LOADMODE_NOISE; break;
    case 2: load_mode = LOADMODE_REMOTE; break;
  }
}


void
load_append(load_definition *tload)
{
  GtkTreeIter iter;
  GtkTreeModel *ldstore;
  
  ldstore = gtk_tree_view_get_model(GTK_TREE_VIEW(ld_view));
  gtk_list_store_append(GTK_LIST_STORE(ldstore), &active_ld_iter);

  tload->pt = create_load_point();
  load_update(tload);
}


void
enable_load_buttons(int action)
{
  switch (load_mode) {
    case LOADMODE_R_JX: enable_load_rx_buttons(action); break;
    case LOADMODE_NOISE: enable_load_nb_buttons(action); break;
  }
}


void
load_update(load_definition *tload)
{
  GtkListStore *store;
  char r[20], x[20], f[20]; 
  
  if (active_ld_iter.stamp == 0) return;
  store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(ld_view)));

  sprintf(f, "%.*f", pref.prec_mhz, tload->f);
  sprintf(r, "%.1f", tload->r);
  sprintf(x, "%.1f", tload->x);
  set_point(tload->pt, tload->r + tload->x*I, ZPT_LOAD);
  gtk_list_store_set(store, &active_ld_iter,
                LD_FREQ, f,
                LD_REAL, r,
                LD_REACT, x,
                LD_FREQ_VAL, tload->f,
                LD_REAL_VAL, tload->r,
                LD_REACT_VAL, tload->x,
                LD_NB_RVAL, tload->nb_r,  
                LD_NB_CVAL, tload->nb_c,  
                LD_NB_EXT, tload->nb_ext,
                LD_POINT, tload->pt,
                -1);

  enable_load_buttons(LOADLIST_UPDATED);
}


void
on_load_selected(GtkTreeView *treeview)
{
  switch (load_mode) {
    case LOADMODE_R_JX:
      on_load_rx_selected(treeview);
      break;
    case LOADMODE_NOISE:
      on_load_nb_selected(treeview);
      break;
  }
}


void
loadlist_clear(void)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  gboolean more;
  chart_point *pt;

  model = gtk_tree_view_get_model(GTK_TREE_VIEW(ld_view));
  more  = gtk_tree_model_get_iter_first(model, &iter);

  while (more) {
    gtk_tree_model_get(model, &iter, 
		LD_POINT, &pt,
		-1);
    if (pt) {
      gtk_object_destroy(GTK_OBJECT(pt->point));
      g_free(pt);
    } 
    more = gtk_tree_model_iter_next(model, &iter);
  }
  gtk_list_store_clear(GTK_LIST_STORE(model));

  switch (load_mode) {
    case LOADMODE_R_JX:
      loadlist_rx_clear(); break;
    case LOADMODE_NOISE:
      loadlist_nb_clear(); break;
//    case LOADMODE_REMOTE:
//      loadlist_rem_clear(); break;
  }
}


void
load_modified(int which)
{
  enable_load_buttons(LOADLIST_EDITED);
}


/*
 *	File operations
 */

gboolean
save_each_load(GtkTreeModel *model,
               GtkTreePath  *path,
               GtkTreeIter  *iter,
               gpointer      user_data)
{
  load_definition load;
  FILE *xmlf = user_data;
                                         
  gtk_tree_model_get(model, iter,
		LD_FREQ_VAL, &load.f,
		LD_REAL_VAL, &load.r,
		LD_REACT_VAL, &load.x,
		LD_NB_RVAL, &load.nb_r,
		LD_NB_CVAL, &load.nb_c,
		LD_NB_EXT, &load.nb_ext,
		-1);

  fprintf(xmlf, " <load f=\"%f\" r=\"%f\" x=\"%f\" "
		"nb_r=\"%f\" nb_c=\"%f\" nb_ext=\"%d\" />\n",
                load.f, load.r, load.x,
		load.nb_r, load.nb_c, load.nb_ext);

  return FALSE;
}


void
save_loads(char *fn)
{
  GtkWidget *loads = lookup_widget(MainWindow, "load_treeview");
  GtkTreeModel *store = gtk_tree_view_get_model(GTK_TREE_VIEW(loads));
  FILE *xmlf;
    
  if ((xmlf = fopen(fn, "r"))) {
    fclose(xmlf);
    if (ok_cancel_dialog(_("Confirmation"), 
			 _("File exists - want to overwrite?"))
        == 1) {
      return;
    }
  }
  xmlf = fopen(fn, "w");
  if (!strcmp(fn, "")) return;
  fprintf(xmlf, "<?xml version=\"1.0\"?>\n");
  fprintf(xmlf, "<loads>\n");
  gtk_tree_model_foreach(store, save_each_load, xmlf);
  fprintf(xmlf, "</loads>\n");
  fclose(xmlf);
}


void
load_loads(char *fn)
{
  load_definition load;
  xmlChar *dbl;
  xmlDocPtr doc;
  xmlNodePtr cur;

  if (!xml_file_loaded(&doc, fn, "loads")) return;

  cur = xmlDocGetRootElement(doc);
  cur = cur->xmlChildrenNode;
  while (cur != NULL) {
    if ((!xmlStrcmp(cur->name, (const xmlChar *) "load"))) {
      active_load.f = atof(dbl = xmlGetProp(cur, "f")); xmlFree(dbl);

      active_load.r = atof(dbl = xmlGetProp(cur, "r")); xmlFree(dbl);

      active_load.x = atof(dbl = xmlGetProp(cur, "x")); xmlFree(dbl);

      if (dbl = xmlGetProp(cur, "nb_r")) {
        active_load.nb_r = atof(dbl); xmlFree(dbl);
      } else
        active_load.nb_r = 0;

      if (dbl = xmlGetProp(cur, "nb_c")) {
        active_load.nb_c = atof(dbl); xmlFree(dbl);
      } else
        active_load.nb_c = 0;

      if (dbl = xmlGetProp(cur, "nb_ext")) {
        active_load.nb_ext = atoi(dbl); xmlFree(dbl);
      } else
        active_load.nb_ext = 0;

      load_append(&active_load);
    }
    cur = cur->next;
  }
	
  xmlFreeDoc(doc);
}

