/*
 *  global.c: A few global routines - mostly configuration
 *
 *  Copyright (C) 1997- John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "support.h"
#include "global.h"
#include "confmgr.h"

pref_struct     pref;
char		*initial_chart = PACKAGE_DATA_DIR"/pixmaps/smith2.png";

conf_definition preftable[] = {
  // Structure is:
  //	char typ, 	Widget type
  //	     *wdg;	Widget name
  //	void *data;	Field pointer in preference structure
  //	int misc;	Misc values
  //	char styp, 	Variable type
  //	     *key;	Key name in config file
  //	char isdef,	
  //	     *def;	Default value

  // General
  {CFM_W_SPBTN_INT,	    "conf_prec_mhz_spbtn",		    &pref.prec_mhz, 0,
   CFM_T_INT,		    "global/prec_mhz",		        0, "3"},
  {CFM_W_SPBTN_INT,	    "conf_prec_imp_spbtn",		    &pref.prec_imp, 0,
   CFM_T_INT,		    "global/prec_imp",		        0, "3"},
  {CFM_W_SPBTN_INT,     "conf_prec_adm_spbtn",		    &pref.prec_adm, 0,
   CFM_T_INT,           "global/prec_adm",		        0, "6"},
  {CFM_W_CBTN,          "conf_always_imp_cbtn",		    &pref.always_imp, 0,
   CFM_T_CHAR,          "global/always_imp",		    0, "1"},

  {CFM_W_ENTRY_DOUBLE,  "conf_swr_entry",		        &pref.swr_circle, 2,
   CFM_T_DOUBLE,        "global/swr_circle",		    0, "2.0"},
  {CFM_W_COLORBUTTON,   "conf_swrcircle_colbtn",	    &pref.swr_color, 0,
   CFM_T_INT,           "aspect/swr_color",		        0, "-16776961"},
  {CFM_W_CBTN,          "conf_swr_cbtn",		        &pref.show_swr, 0,
   CFM_T_CHAR,          "global/show_swr",		        0, "1"},
  {CFM_W_ENTRY_STR,     "conf_complex_entry",		    &pref.complex_sfx, 0,
   CFM_T_STR,           "global/complex_suffix",	    0, "j"},

  {CFM_W_CBTN,          "conf_g1_cbtn",			        &pref.show_g1, 0,
   CFM_T_CHAR,          "global/show_g1",		        0, "1"},
  {CFM_W_COLORBUTTON,   "conf_g1circle_colbtn",		    &pref.g1_color, 0,
   CFM_T_INT,           "aspect/g1_color",		        0, "-1381126657"},
  {CFM_W_CBTN,          "use_bitmap_cbtn",		        &pref.use_bitmap, 0,
   CFM_T_CHAR,          "global/use_bitmap",		    0, "0"},

  {CFM_W_FONTBUTTON,    "conf_cursor_font_fbtn",	    &pref.cursor_font, 0,
   CFM_T_STR,           "aspect/cursor_font",		    0, "Courier New 8"}, 
  {CFM_W_FONTBUTTON,    "conf_results_font_fbtn",	    &pref.results_font, 0,
   CFM_T_STR,           "aspect/results_font",		    0, "Courier New 8"}, 

  // screen plot colors & sizes

  {CFM_W_SPBTN_INT,     "conf_load_spbtn",		        &pref.point_size[ZPT_LOAD], 0,
   CFM_T_INT,           "aspect/load_size",		        0, "8"},
  {CFM_W_SPBTN_INT,     "conf_point_spbtn",		        &pref.point_size[ZPT_INT], 0,
   CFM_T_INT,           "aspect/point_size",		    0, "6"},
  {CFM_W_SPBTN_INT,     "conf_final_spbtn",		        &pref.point_size[ZPT_FINAL], 0,
   CFM_T_INT,           "aspect/final_size",		    0, "8"},

  {CFM_W_COLORBUTTON,   "conf_load_colbtn",		        &pref.point_color[ZPT_LOAD], 0,
   CFM_T_INT,           "aspect/load_color",		    0, "0xffff00ff"},
  {CFM_W_COLORBUTTON,   "conf_point_colbtn",		    &pref.point_color[ZPT_INT], 0,
   CFM_T_INT,           "aspect/point_color",		    0, "0x00ff00ff"},
  {CFM_W_COLORBUTTON,   "conf_final_colbtn",		    &pref.point_color[ZPT_FINAL], 0,
   CFM_T_INT,           "aspect/final_color",		    0, "0xff0400ff"},

  {CFM_W_COLORBUTTON,   "conf_z_arc_colbtn",		    &pref.arc_color[Z_ARC], 0,
   CFM_T_INT,           "aspect/z_arc_color",		    0, "0xff7474ff"},
  {CFM_W_SPBTN_DOUBLE,  "conf_z_arc_spbtn",		        &pref.arc_size[Z_ARC], 0,
   CFM_T_DOUBLE,	    "aspect/z_arc_width",	        0, "1.0"},
   
  {CFM_W_COLORBUTTON,   "conf_y_arc_colbtn",		    &pref.arc_color[Y_ARC], 0,
   CFM_T_INT,           "aspect/y_arc_color",		    0, "0x808080ff"},
  {CFM_W_SPBTN_DOUBLE,  "conf_y_arc_spbtn",		        &pref.arc_size[Y_ARC], 0,
   CFM_T_DOUBLE,	    "aspect/y_arc_width",           0, "1.0"},

  {CFM_W_COLORBUTTON,   "conf_line_arc_colbtn",		    &pref.arc_color[K_ARC], 0,
   CFM_T_INT,           "aspect/line_arc_color",	    0, "0x5ece5eff"},
  {CFM_W_SPBTN_DOUBLE,  "conf_line_arc_spbtn",		    &pref.arc_size[K_ARC], 0,
   CFM_T_DOUBLE,	    "aspect/line_arc_width",        0, "1.0"},

  {CFM_W_COLORBUTTON,   "conf_zy_line_colbtn",		    &pref.arc_color[ZY_LINE], 0,
   CFM_T_INT,           "aspect/zy_line_color",		    0, "0xa7a7a7ff"},
  {CFM_W_SPBTN_DOUBLE,  "conf_zy_line_spbtn",		    &pref.arc_size[ZY_LINE], 0,
   CFM_T_DOUBLE,	    "aspect/zy_line_width",	        0, "1.0"},

  {CFM_W_COLORBUTTON,   "conf_r_arc_colbtn",		    &pref.arc_color[R_ARC], 0,
   CFM_T_INT,           "aspect/r_arc_color",		    0, "0xc0c0ffff"},
  {CFM_W_COLORBUTTON,   "conf_r_arc_bold_colbtn",	    &pref.arc_color[RB_ARC], 0,
   CFM_T_INT,           "aspect/rb_arc_color",		    0, "0x8080ffff"},

  {CFM_W_COLORBUTTON,   "conf_x_arc_colbtn",		    &pref.arc_color[X_ARC], 0,
   CFM_T_INT,           "aspect/x_arc_color",		    0, "0xc0c0ffff"},
  {CFM_W_COLORBUTTON,   "conf_x_arc_bold_colbtn",	    &pref.arc_color[XB_ARC], 0,
   CFM_T_INT,           "aspect/xb_arc_color",   	    0, "0x8080ffff"},

  {CFM_W_COLORBUTTON,	"conf_g1_colbtn",               &pref.g1_color, 0,
   CFM_T_INT,           "aspect/g1_color",              0, "0x808080ff"},
  {CFM_W_COLORBUTTON,   "conf_swr_colbtn",              &pref.swr_color, 0,
   CFM_T_INT,           "aspect/swr_color",             0, "0xff8080ff"},

  // [29] Screen bitmap background
  {CFM_W_ENTRY_STR,     "conf_chartbg_entry",           &pref.chart, 0,
   CFM_T_STR,           "chart/chartbg",                0,
                        PACKAGE_DATA_DIR"/pixmaps/linsmith/smith4.png"},
  {CFM_W_ENTRY_DOUBLE,  "conf_radius_entry",            &pref.chartradius, 1,
   CFM_T_DOUBLE,        "chart/radius",                 0,      "211"},
  {CFM_W_ENTRY_DOUBLE,  "conf_xoffs_entry",             &pref.x_offs, 1,
   CFM_T_DOUBLE,        "chart/x_offs",                 0,      "2"},
  {CFM_W_ENTRY_DOUBLE,  "conf_yoffs_entry",             &pref.y_offs, 1,
   CFM_T_DOUBLE,        "chart/y_offs",                 0,      "0"},

  // [33] Screen vector background
  {CFM_W_COLORBUTTON,   "conf_vecbg_colbtn",	        &pref.vec_background, 0,
   CFM_T_INT,           "aspect/vecbg_color",           0,      "0xf0ffffff"},
  {CFM_W_ENTRY_INT,	    "conf_vecbg_size_entry",        &pref.vec_bg_radius, 0,
   CFM_T_INT,		    "aspect/vecbg_size",            0,      "250"},

  {CFM_W_FONTBUTTON,    "conf_font_fpick",              &pref.vec_font_name, 0,
   CFM_T_STR,           "aspect/font",                  0,      "Helvetica 8"},
  {CFM_W_COLORBUTTON,   "conf_font_colbtn",             &pref.vec_font_color, 0,
   CFM_T_INT,           "aspect/font_color",            0,      "0xff7373ff"},

  // [37] Printer setup
  {CFM_W_COMBOBOX_STR,  "prt_papersize_cbbox",          &pref.prt_papersize, 0,
   CFM_T_STR,           "print/papersize",              0,      "A4"},
  {CFM_W_ENTRY_DOUBLE,  "prt_chartsize_entry",          &pref.prt_chartsize, 1,
   CFM_T_DOUBLE,        "print/chartsize",              0,      "160.0"},
  {CFM_W_ENTRY_DOUBLE,  "prt_mleft_entry",              &pref.prt_mleft, 1,
   CFM_T_DOUBLE,        "print/marginleft",             0,      "1.0"},
  {CFM_W_ENTRY_DOUBLE,  "prt_mbottom_entry",            &pref.prt_mbottom, 1,
   CFM_T_DOUBLE,        "print/marginbottom",           0,      "1.0"},

  // [41] Printer chart background properties

  // R arcs - normal
  {CFM_W_COLORBUTTON,   "prt_r_arc_colbtn",             &pref.prt_arc_color[R_ARC],  0,
   CFM_T_INT,           "print/bgarc_color_r",          0,      "0xffb95aff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_r_arc_spbtn",              &pref.prt_arc_size[R_ARC],   0,
   CFM_T_DOUBLE,        "print/bgarc_size_r",           0,      "0.1"},

  // R arcs - bold
  {CFM_W_COLORBUTTON,   "prt_r_arc_bold_colbtn",        &pref.prt_arc_color[RB_ARC], 0,
   CFM_T_INT,           "print/bgarc_bold_color_r",     0,      "0xfa9002ff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_r_arc_bold_spbtn",         &pref.prt_arc_size[RB_ARC],  0,
   CFM_T_DOUBLE,        "print/bgarc_bold_size_r",      0,      "0.2"},

  // X arcs - normal
  {CFM_W_COLORBUTTON,   "prt_x_arc_colbtn",             &pref.prt_arc_color[X_ARC],  0,
   CFM_T_INT,           "print/bgarc_color_x",          0,      "0xffa5a5ff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_x_arc_spbtn",              &pref.prt_arc_size[X_ARC],   0,
   CFM_T_DOUBLE,        "print/bgarc_size_x",           0,      "0.1"},

  // X arcs - bold
  {CFM_W_COLORBUTTON,   "prt_x_arc_bold_colbtn",        &pref.prt_arc_color[XB_ARC],	0,
   CFM_T_INT,           "print/bgarc_bold_color_x",	    0,      "0xff6060ff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_x_arc_bold_spbtn",         &pref.prt_arc_size[XB_ARC],	0,
   CFM_T_DOUBLE,        "print/bgarc_bold_size_x",	    0,      "0.2"},

  {CFM_W_COLORBUTTON,	"prt_g1_colbtn",		        &pref.prt_g1_color, 0,
   CFM_T_INT,           "print/g1_color",		        0,      "0x808080ff"},
  {CFM_W_COLORBUTTON,   "prt_swr_colbtn",		        &pref.prt_swr_color, 0,
   CFM_T_INT,           "print/swr_color",		        0,      "0xff7a7aff"},

  // Actual (print) plot properties - load, intermediate, source points

  // Load points
  {CFM_W_COLORBUTTON,   "prt_ldfill_colbtn",            &pref.prt_fill_color[ZPT_LOAD], 0,
   CFM_T_INT,           "print/ldfill_color",		0,      "0xffff00ff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_lddiam_spbtn",             &pref.prt_diam[ZPT_LOAD],	0,
   CFM_T_DOUBLE,        "print/lddiam",			0,      "2.0"},
  {CFM_W_COLORBUTTON,   "prt_ldbrdr_colbtn",            &pref.prt_brdr_color[ZPT_LOAD], 0,
   CFM_T_INT,           "print/ldbrdr_color",		0,      "0xfff699b4"},
  {CFM_W_SPBTN_DOUBLE,  "prt_ldbrdr_spbtn",             &pref.prt_brdr[ZPT_LOAD],	0,
   CFM_T_DOUBLE,        "print/ldbrdr",			0,      "0.2"},

  // Intermediate points
  {CFM_W_COLORBUTTON,   "prt_intfill_colbtn",           &pref.prt_fill_color[ZPT_INT],	0,
   CFM_T_INT,           "print/intfill_color",		0,      "0x00ff00ff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_intdiam_spbtn",            &pref.prt_diam[ZPT_INT],	0,
   CFM_T_DOUBLE,        "print/intdiam",		0,      "1.0"},
  {CFM_W_COLORBUTTON,   "prt_intbrdr_colbtn",           &pref.prt_brdr_color[ZPT_INT],	0,
   CFM_T_INT,           "print/intbrdr_color",		0,      "0x006e06ff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_intbrdr_spbtn",            &pref.prt_brdr[ZPT_INT],	0,
   CFM_T_DOUBLE,        "print/intbrdr",		0,      "0.1"},

  // Final points
  {CFM_W_COLORBUTTON,   "prt_finalfill_colbtn",         &pref.prt_fill_color[ZPT_FINAL], 0,
   CFM_T_INT,           "print/finalfill_color",        0,      "0xff0000ff"},
  {CFM_W_COLORBUTTON,   "prt_finalbrdr_colbtn",         &pref.prt_brdr_color[ZPT_FINAL], 0,
   CFM_T_INT,           "print/finalbrdr_color",        0,      "0xb40000ff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_finaldiam_spbtn",          &pref.prt_diam[ZPT_FINAL],   0,
   CFM_T_DOUBLE,        "print/finaldiam",              0,      "2.0"},
  {CFM_W_SPBTN_DOUBLE,  "prt_finalbrdr_spbtn",          &pref.prt_brdr[ZPT_FINAL],   0,
   CFM_T_DOUBLE,        "print/finalbrdr",              0,      "0.2"},


  // Actual plot properties - arcs
  // Z - arcs
  {CFM_W_COLORBUTTON,   "prt_z_arc_colbtn",             &pref.prt_arc_color[Z_ARC],  0,
   CFM_T_INT,           "print/arc_color_z",            0,      "0xff9ba5ff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_z_arc_spbtn",              &pref.prt_arc_size[Z_ARC],   0,
   CFM_T_DOUBLE,        "print/arc_z",                  0,      "0.4"},

  // Y - arcs
  {CFM_W_COLORBUTTON,   "prt_y_arc_colbtn",             &pref.prt_arc_color[Y_ARC],  0,
   CFM_T_INT,           "print/arc_color_y",            0,      "0xb7b7ffff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_y_arc_spbtn",              &pref.prt_arc_size[Y_ARC],   0,
   CFM_T_DOUBLE,        "print/arc_y",                  0,      "0.4"},

  // Line - arcs
  {CFM_W_COLORBUTTON,   "prt_line_arc_colbtn",          &pref.prt_arc_color[K_ARC],  0,
   CFM_T_INT,           "print/arc_color_line",         0,      "0xb1ffb1ff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_line_arc_spbtn",           &pref.prt_arc_size[K_ARC],   0,
   CFM_T_DOUBLE,        "print/arc_line",               0,      "0.4"},

  // Z-Y connection lines
  {CFM_W_COLORBUTTON,   "prt_zy_line_colbtn",           &pref.prt_arc_color[ZY_LINE],   0,
   CFM_T_INT,           "print/arc_color_zyline",       0,      "0xb9b9b9ff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_zy_line_spbtn",            &pref.prt_arc_size[ZY_LINE],    0,
   CFM_T_DOUBLE,        "print/arc_zyline",             0,      "0.1"},

  // Misc
  // SWR circle
  {CFM_W_COLORBUTTON,   "prt_swrcircle_colbtn",         &pref.prt_swr_color,    0,
   CFM_T_INT,           "print/swr_color",              0,      "-0xff7a7aff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_swrcircle_spbtn",          &pref.prt_swr_size,     0,
   CFM_T_DOUBLE,        "print/swr_size",               0,      "0.1"},

  // G=1 circle
  {CFM_W_COLORBUTTON,   "prt_g1circle_colbtn",          &pref.prt_g1_color,     0,
   CFM_T_INT,           "print/g1_color",               0,      "0xc3c3c3ff"},
  {CFM_W_SPBTN_DOUBLE,  "prt_g1circle_spbtn",           &pref.prt_g1_size,      0,
   CFM_T_DOUBLE,        "print/g1_size",                0,      "0.1"},

  // Font
  {CFM_W_FONTBUTTON,    "prt_fontbtn",			&pref.prt_font_name,	0,
   CFM_T_STR,           "print/prt_font_name",		0,	"Helvetica"},
  {CFM_W_COLORBUTTON,   "prt_font_colbtn",		&pref.prt_font_color,	0,
   CFM_T_INT,           "print/font_color",		0,	"-0xbe8f00f"},
  {CFM_W_SPBTN_DOUBLE,  "prt_fontsize_spbtn",		&pref.prt_font_size,	0,
   CFM_T_DOUBLE,        "print/font_size",		0,	"2.0"},

  // Remote tab: control parameters
  {CFM_W_CBOX,		"confrem_mode_cbox",		&pref.rem_mode,		0,
   CFM_T_INT,		"remote/mode",			0,	"0"},
  {CFM_W_ENTRY_STR,	"confrem_outpipe_entry",        &pref.rem_outpipe,	0,
   CFM_T_STR,		"remote/outpipe",		0,	"/tmp/linsmith.out"},
  {CFM_W_ENTRY_STR,	"confrem_inpipe_entry",         &pref.rem_inpipe,	0,
   CFM_T_STR,		"remote/inpipe",		0,	"/tmp/linsmith.in"},
  {CFM_W_ENTRY_INT,	"confrem_timeout_entry",	&pref.rem_timeout,	0,
   CFM_T_INT,		"remote/timeout",		0,	"2000"},
  {CFM_W_ENTRY_STR,	"confrem_serial_port_entry",    &pref.rem_serial_port,	0,
   CFM_T_STR,		"remote/serial_port",		0,	"/dev/ttyS1"},
   

  // Noisebridge tab:
  {CFM_W_ENTRY_DOUBLE,  "conf_nb_offs_entry",           &pref.nb_offset,        2,
   CFM_T_DOUBLE,        "noisebr/offset",               0,      "110.0"},
  {CFM_W_ENTRY_DOUBLE,  "conf_nb_ext_entry",            &pref.nb_extender,      2,
   CFM_T_DOUBLE,        "noisebr/extender",             0,      "100.0"},
  {CFM_W_CBOX,          "nb_type_cbbox",                &pref.nb_type,          0,
   CFM_T_INT,           "noisebr/type",                 0,      "0"},

  // Files tab: last used filenames
  {CFM_W_ENTRY_STR,     "conf_last_ld_entry",           &pref.last_ld_file,     0,
   CFM_T_STR,           "files/last_load_file",         0,      ""},
  {CFM_W_ENTRY_STR,     "conf_last_el_entry",           &pref.last_el_file,     0,
   CFM_T_STR,           "files/last_elem_file",         0,      ""},
  {CFM_W_ENTRY_STR,     "conf_last_ps_entry",           &pref.last_ps_file,     0,
   CFM_T_STR,           "files/last_ps_file",           0,      ""},
  {CFM_W_ENTRY_STR,     "conf_last_csv_entry",          &pref.last_csv_file,    0,
   CFM_T_STR,           "files/last_csv_file",          0,      ""},
  {CFM_W_ENTRY_STR,     "conf_last_s2p_entry",          &pref.last_s2p_file,    0,
   CFM_T_STR,           "files/last_s2p_file",          0,      ""},

  // File tab: CSV separator character
  {CFM_W_ENTRY_STR,	    "conf_csv_sep_entry", 	&pref.csv_sep,          0,
   CFM_T_STR,		    "files/csv_sep",	        0,      ","},

  {CFM_W_LAST}
};

conf_definition pathtable[] = {
  // Common
  {CFM_W_ENTRY_STR,     "conf_last_ld_entry",           &pref.last_ld_file,     0,
   CFM_T_STR,           "files/last_load_file",         0,      ""},
  {CFM_W_ENTRY_STR,     "conf_last_el_entry",           &pref.last_el_file,     0,
   CFM_T_STR,           "files/last_elem_file",         0,      ""},
  {CFM_W_ENTRY_STR,     "conf_last_ps_entry",           &pref.last_ps_file,     0,
   CFM_T_STR,           "files/last_ps_file",           0,      ""},

  {CFM_W_LAST}
};
#define pathlen (sizeof(pathtable)/sizeof(conf_definition))

void
save_config(void)
{
  save_by_table(NULL, preftable);
}

void
save_file_paths(void)
{
  save_by_table(NULL, pathtable);
}

void
load_config(void)
{
  load_by_table(NULL, preftable);
}


void
parse_configwindow(GtkWidget *ref)
{
  parse_widget_by_table(preftable, ref);
}

void
load_combo_box_from_glist(GtkComboBox *cbbox, GList *list)
{
  GtkListStore *store;
  GtkTreeIter iter;
  GtkPaperSize *ps;
  GList *l;

  store = gtk_list_store_new(2, G_TYPE_STRING, G_TYPE_STRING);
  gtk_combo_box_set_model(cbbox, GTK_TREE_MODEL(store));
  for (l = g_list_first(list); l; l = l->next) {
    ps = (GtkPaperSize *) l->data;
    gtk_list_store_append(store, &iter);
    gtk_list_store_set(store, &iter, 
                0, gtk_paper_size_get_display_name(ps),
                1, gtk_paper_size_get_name(ps),
                -1);
  }

  gtk_combo_box_set_active(cbbox, 0);
}

void
load_configwindow(GtkWidget *ref)
{
  GtkWidget *w;
  GList *list;
  
  w = lookup_widget(ref, "prt_papersize_cbbox");
  list = gtk_paper_size_get_paper_sizes(TRUE);
  load_combo_box_from_glist(GTK_COMBO_BOX(w), list);
  
  load_widget_by_table(preftable, ref);
}
