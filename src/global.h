/*
 *  global.c: A few global routines - mostly configuration
 *
 *  Copyright (C) 1997-2005 John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include "types.h"

#define prog_name       "linsmith"
#define prog_title	    N_("Smith Chart")
#define prog_version    VERSION
#define prog_author     "John Coppens"
#define prog_right      "(c) John Coppens 1997-"
#define prog_com        ""

enum {
	DBG_LOADS	    = 1,
	DBG_ELEMENTS	= 2,
	DBG_GRAPHICS 	= 4,
	DBG_PRINTING	= 8,
	DBG_MATH	    = 16,
	DBG_LOGGING	    = 32
};

enum {TO_GENERATOR, TO_LOAD};

typedef struct {
  // general parameters
  char  last_el_file[200],
        last_ld_file[200],
        last_ps_file[200],
        last_csv_file[200],
	last_s2p_file[200],
        chart[200],

        cursor_font[80],
        results_font[80],

	complex_sfx[2],
  	show_swr,           // Flags
	show_g1,
	always_imp,         // TRUE: No Z/Y conversion in results
	use_bitmap;         // TRUE: use bitmap else vector chart

  int   prec_mhz,
	prec_imp,
	prec_adm,
        nb_type,
        rotation;           // Direction of rotation

  double chart_z0,
  	swr_circle,
  	nb_offset,	    // Offset capacitance
  	nb_extender;        // Extender resistor value

  // screen configuration

  char  vec_font_name[80];

  int   arc_color[ARC_TYPES],
        point_size[PT_TYPES],	    // Size of imp/adm marks
        point_color[PT_TYPES],	    // Color of imp/adm marks
        swr_color,                  // SWR circle color
        g1_color,                   // G=1 circle color
	vec_bg_radius,              // Vector chart radius in pixels
	vec_background,             // Vector chart background color
	vec_font_color;             // Vector chart font color

  double arc_size[ARC_TYPES],	    // Screen line width of arcs
	x_offs,                     // for bitmap backgrounds
  	y_offs,
        chartradius;

  // printer configuration

  char  prt_papersize[80],
	prt_font_name[80];

  int   prt_arc_color[ARC_TYPES],
        prt_fill_color[PT_TYPES],
        prt_brdr_color[PT_TYPES],
        prt_swr_color,
        prt_g1_color,
        prt_font_color;

  double prt_chartsize,
        prt_diam[PT_TYPES],
        prt_brdr[PT_TYPES],
        prt_arc_size[ARC_TYPES],

        prt_mleft,
        prt_mbottom,
        prt_font_size,
        prt_swr_size,
        prt_g1_size;

  // Remote control parameters
  char  rem_outpipe[120],
	rem_inpipe[120],
	rem_serial_port[120],
  	rem_mode;
  int   rem_timeout;

  // CSV parameters
  char  csv_sep[2];
  
  // These are local (non-saveable) parameters
  
  int	      inpipe, outpipe;	// Input/output fifos for remote instrument
  GIOChannel *inchannel,
             *outchannel;
} pref_struct;

extern	    pref_struct pref;

void        parse_configwindow(GtkWidget *ref);
void        load_configwindow(GtkWidget *ref);

void        save_config(void);
void        load_config(void);
void		save_file_paths(void);
