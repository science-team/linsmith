// cables.h
//
// Copyright (C) 2012 - Unknown
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <gtk/gtk.h>

enum   {SEP_IND_VAL, SEP_CAP_VAL,
	SEP_STUB_VAL, SEP_STUB_Z0, 
	SEP_LINE_VAL, SEP_LINE_Z0,
	SEP_XFORM_VAL};

enum {  CBLTBL_NAME,
	CBLTBL_Z0,              // Z0
	CBLTBL_VF,              // Velocity factor
	CBLTBL_DB1,             // Loss at a first frequency
	CBLTBL_MHZ1,
	CBLTBL_DB2,             // Loss at a second frequency
	CBLTBL_MHZ2,
	CBLTBL_I,
        CBLTBL_COLS
};

void	load_cable_table(GtkWidget *cmb);
void	show_cable_data(int apply_to, int cable_nr);
int	get_selected_cable_nr(GtkWidget *ref);
