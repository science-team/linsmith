typedef struct {
  char type;
  double val, a_min, a_max, zm_min, zm_max;
  int priority;
  char bold;
} plot_definition;

enum {LBL_NE, LBL_SE, LBL_SW, LBL_NW};

typedef struct {
  char type;
  double re, im;
  char pos;
} label_definition;

extern int nrpldefs, nrlbldefs;
extern plot_definition pldef[];
extern label_definition lbldef[];
