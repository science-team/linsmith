#ifndef MYTYPES_H
#define MYTYPES_H

#include <gtk/gtk.h>
#include <gnome.h>
#include <complex.h>

enum {ZOOM_IN, ZOOM_OUT};

enum {ZPT_LOAD, ZPT_INT, ZPT_FINAL,
      PT_TYPES};

enum {Z_ARC,  Y_ARC,  K_ARC,
      R_ARC,  X_ARC,
      RB_ARC, XB_ARC,
      ZY_LINE,
      ARC_TYPES};

typedef struct {
  complex          value;
  char             line,
                   fixed;
  int              style,
                   handler;
  GnomeCanvasItem  *point;
  GnomeCanvasGroup *group;
} chart_point;

typedef chart_point *chart_pt;
typedef GList *chart_ptlist;

typedef struct {
  GnomeCanvasItem  *line;
  GnomeCanvasGroup *group;
} chart_line;

typedef chart_line *chart_ln;
typedef GList *chart_lnlist;

typedef struct {
  double f, r, x, nb_r, nb_c;
  char nb_ext;
  chart_pt pt;
} load_definition;

typedef struct {
  int conn, typ, useloss;
  double val1, val2, z0, vf, loss1, loss2, mhz1, mhz2;
  chart_ptlist ptlst;
  chart_lnlist lnlst;
} el_definition;
    
#endif
