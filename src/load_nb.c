/*
 *  load_nb.c: Noise bridge related input functions etc.
 *
 *  Copyright (C) 1997-2005 John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <libxml/tree.h>
#include <gnome.h>
#include <unistd.h>

#include "support.h"
#include "callbacks.h"
#include "printer.h"
#include "global.h"
#include "main.h"
#include "load.h"
#include "load_nb.h"
#include "chart.h"
#include "misc.h"

GtkTreeIter	active_ld_iter;
load_definition	active_load;
GtkWidget	*ld_view;

//---------------------------------------------------------------------
//	Loads
//---------------------------------------------------------------------

void
enable_load_nb_buttons(int action)
{
  GtkWidget *w, *w1;

  w = lookup_widget(MainWindow, "load_nb_append_btn");
  gtk_widget_set_sensitive(w,
       load_nb_valid(NULL)
  );

  w = lookup_widget(MainWindow, "load_remove_btn");   
  gtk_widget_set_sensitive(w,
       action == LOADLIST_UPDATED
    || action == LOADLIST_EDITED
    || action == LOADLIST_SELECT
  );

  w = lookup_widget(MainWindow, "load_nb_upd_btn");
  gtk_widget_set_sensitive(w,
       (action == LOADLIST_EDITED)
    && load_nb_valid(NULL)
    && (number_selected_loads() == 1)
  );

  w = lookup_widget(MainWindow, "load_clear_btn");
  gtk_widget_set_sensitive(w,
	action = LOADLIST_EMPTY
  );

  if (action == LOADLIST_UPDATED ||
      action == LOADLIST_REMOVED)
    recalculate_all(DST_SCREEN);
}


void
on_load_nb_selected(GtkTreeView *treeview)
{
  GtkWidget *w1, *w2, *w3, *w4;
  GtkTreeModel *store;
  GtkTreeSelection *sel;
  char bff[20];

  sel = gtk_tree_view_get_selection(treeview);
  if (gtk_tree_selection_get_selected(sel, &store, &active_ld_iter)) {
    gtk_tree_model_get(store, &active_ld_iter, 
		LD_FREQ_VAL,  &active_load.f, 
		LD_REAL_VAL,  &active_load.r, 
		LD_REACT_VAL, &active_load.x,
		LD_NB_RVAL,   &active_load.nb_r,
		LD_NB_CVAL,   &active_load.nb_c,
		LD_NB_EXT,    &active_load.nb_ext,
		LD_POINT,     &active_load.pt,
		-1);

    w1 = lookup_widget(MainWindow, "load_nb_r_entry");
    w2 = lookup_widget(MainWindow, "load_nb_c_entry");
    w3 = lookup_widget(MainWindow, "load_nb_freq_entry");
    w4 = lookup_widget(MainWindow, "load_nb_ext_cbtn");

    sprintf(bff, "%.2f", active_load.nb_r);
    gtk_entry_set_text(GTK_ENTRY(w1), bff);

    sprintf(bff, "%.2f", active_load.nb_c);
    gtk_entry_set_text(GTK_ENTRY(w2), bff);

    sprintf(bff, "%.*f", pref.prec_mhz, active_load.f);
    gtk_entry_set_text(GTK_ENTRY(w3), bff);

    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(w4), active_load.nb_ext);

//    enable_load_buttons(LOADLIST_SELECT);
  }
}


int
load_nb_valid(load_definition *dest)
{
  GtkWidget *w;
  const char *txt = NULL;
  double xc;
  load_definition tload;

  // Check the frequency (valid syntax and non-zero)
  w = lookup_widget(MainWindow, "load_nb_freq_entry");
  txt = gtk_entry_get_text(GTK_ENTRY(w));
  if (strlen(txt) == 0) return FALSE;

  errno = 0;
  tload.f = strtod(txt, NULL);
  if ((tload.f == 0) || (errno != 0)) return FALSE;

  // Then the resistive part of the reading
  w = lookup_widget(MainWindow, "load_nb_r_entry");
  txt = gtk_entry_get_text(GTK_ENTRY(w));
  if (strlen(txt) == 0) return FALSE;

  errno = 0;
  tload.nb_r = strtod(txt, NULL);
  if (errno != 0) return FALSE;

  // And the capacitor reading
  w = lookup_widget(MainWindow, "load_nb_c_entry");
  txt = gtk_entry_get_text(GTK_ENTRY(w));
  if (strlen(txt) == 0) return FALSE;

  errno = 0;
  tload.nb_c = strtod(txt, NULL);
  if (errno != 0) return FALSE;

  w = lookup_widget(MainWindow, "load_nb_ext_cbtn");
  tload.nb_ext = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w));

  if (tload.f == 0) return FALSE;
  if (tload.nb_c == pref.nb_offset)
    tload.nb_c = 0.01;

  if (dest) {
    dest->nb_r = tload.nb_r;
    dest->nb_c = tload.nb_c;
    dest->nb_ext = tload.nb_ext;
    xc = -1e6/(2 * M_PI * tload.f * (tload.nb_c - pref.nb_offset));
    dest->r = tload.nb_r * xc*xc/(xc*xc + tload.nb_r*tload.nb_r);;
    dest->x = tload.nb_r*tload.nb_r * xc/(xc*xc + tload.nb_r*tload.nb_r);;
    dest->f = tload.f;
  }
  return TRUE;
}


void
active_load_nb_update(void)
{
  if (active_ld_iter.stamp == 0) return;
  load_nb_valid(&active_load);
  load_update(&active_load);

  enable_load_nb_buttons(LOADLIST_UPDATED);
}


void
active_load_nb_remove(void)
{
  GtkListStore *store;
  chart_point *pt;

  if (active_ld_iter.stamp == 0) return;

  store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(ld_view)));
  gtk_tree_model_get(GTK_TREE_MODEL(store), &active_ld_iter,
		LD_POINT, &pt,
		-1);

  if (pt) {
    gtk_object_destroy(GTK_OBJECT(pt->point));
    g_free(pt);
  }
                              
  gtk_list_store_remove(store, &active_ld_iter);

  enable_load_buttons(LOADLIST_REMOVED);
  if (number_loads() == 0)
    enable_load_buttons(LOADLIST_EMPTY);
}


void
load_nb_modified(int which)
{
  enable_load_nb_buttons(LOADLIST_EDITED);
}


void
loadlist_nb_clear(void)
{
  enable_load_nb_buttons(LOADLIST_EMPTY);
}
