#include <gtk/gtk.h>

typedef struct {
  float	z0,		// Zo of the data - taken from options or [Reference]
	version;	// Data file version, taken from [Version]
  int	unit,		// Frequency unit, from options
	fmt,		// Data format, from options
	param;		// Parameter type (S, Z, ...) from options
} s2p_dataset;

void	ImportWindow_realize(GtkWidget *widget);

void	s2p_files1_activate(void);
void	import_s2p_cancel_btn_clicked(void);
void	import_s2p_impS11_btn_clicked(void);
void	import_s2p_impS22_btn_clicked(void);

void	import_s2p_sel_invert_btn_clicked(void);
void	import_s2p_sel_none_btn_clicked(void);
void	import_s2p_sel_all_btn_clicked(void);

void	import_csv_activate(void);
