/*
 *  load_rx.c: Standar R+jX Load input functions.
 *
 *  Copyright (C) 1997-2005 John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <libxml/tree.h>
#include <gnome.h>
#include <unistd.h>

#include "support.h"
#include "callbacks.h"
#include "printer.h"
#include "global.h"
#include "main.h"
#include "load.h"
#include "load_rx.h"
#include "chart.h"
#include "misc.h"

GtkTreeIter	active_ld_iter;
load_definition	active_load;
GtkWidget	*ld_view;

//---------------------------------------------------------------------
//	Loads
//---------------------------------------------------------------------

void
enable_load_rx_buttons(int action)
{
  GtkWidget *w, *w1;

  w = lookup_widget(MainWindow, "load_append_btn");
  gtk_widget_set_sensitive(w,
       load_rx_valid(NULL)
  );

  w = lookup_widget(MainWindow, "load_remove_btn");   
  gtk_widget_set_sensitive(w,
       action == LOADLIST_UPDATED
    || action == LOADLIST_EDITED
    || action == LOADLIST_SELECT
  );

  w = lookup_widget(MainWindow, "load_edit_btn");
  gtk_widget_set_sensitive(w,
       (action == LOADLIST_EDITED)
    && load_rx_valid(NULL)
    && (number_selected_loads() == 1)
  );

  w = lookup_widget(MainWindow, "load_clear_btn");
  gtk_widget_set_sensitive(w,
	action = LOADLIST_EMPTY
  );

  if (action == LOADLIST_UPDATED ||
      action == LOADLIST_REMOVED)
    recalculate_all(DST_SCREEN);
}


void
on_load_rx_selected(GtkTreeView *treeview)
{
  GtkWidget *w1, *w2, *w3;
  GtkTreeModel *store;
  GtkTreeSelection *sel;
  char bff[20];

  sel = gtk_tree_view_get_selection(treeview);
  if (gtk_tree_selection_get_selected(sel, &store, &active_ld_iter)) {
    gtk_tree_model_get(store, &active_ld_iter, 
                   LD_FREQ_VAL,  &active_load.f, 
                   LD_REAL_VAL,  &active_load.r, 
                   LD_REACT_VAL, &active_load.x,
                   LD_POINT,     &active_load.pt,
		   -1);

    w1 = lookup_widget(MainWindow, "load_r_entry");
    w2 = lookup_widget(MainWindow, "load_x_entry");
    w3 = lookup_widget(MainWindow, "load_freq_entry");

    sprintf(bff, "%.2f", active_load.r);
    gtk_entry_set_text(GTK_ENTRY(w1), bff);

    sprintf(bff, "%.2f", active_load.x);
    gtk_entry_set_text(GTK_ENTRY(w2), bff);

    sprintf(bff, "%.*f", pref.prec_mhz, active_load.f);
    gtk_entry_set_text(GTK_ENTRY(w3), bff);

//    enable_load_buttons(LOADLIST_SELECT);
  }
}


int
load_rx_valid(load_definition *dest)
{
  GtkWidget *w;
  const char *txt = NULL;
  load_definition tload;

  // Check the frequency
  w = lookup_widget(MainWindow, "load_freq_entry");
  txt = gtk_entry_get_text(GTK_ENTRY(w));
  if (strlen(txt) == 0) return FALSE;

  errno = 0;
  tload.f = strtod(txt, NULL);
  if ((tload.f == 0) || (errno != 0)) return FALSE;

  // Check the real part of the impedance
  w = lookup_widget(MainWindow, "load_r_entry");
  txt = gtk_entry_get_text(GTK_ENTRY(w));
  if (strlen(txt) == 0) return FALSE;

  errno = 0;
  tload.r = strtod(txt, NULL);
  if (errno != 0) return FALSE;

  // Check the imaginary part of the impedance
  w = lookup_widget(MainWindow, "load_x_entry");
  txt = gtk_entry_get_text(GTK_ENTRY(w));
  if (strlen(txt) == 0) return FALSE;

  errno = 0;
  tload.x = strtod(txt, NULL);
  if (errno != 0) return FALSE;

  if (dest) {
    dest->r = tload.r;
    dest->x = tload.x;
    dest->f = tload.f;
  }
  return TRUE;
}


void
load_rx_update(load_definition *tload)
{
  GtkListStore *store;
  char r[20], x[20], f[20];
  
  if (active_ld_iter.stamp == 0) return;
  store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(ld_view)));

  sprintf(f, "%.*f", pref.prec_mhz, tload->f);
  sprintf(r, "%.1f", tload->r);
  sprintf(x, "%.1f", tload->x);
  set_point(tload->pt, tload->r + tload->x*I, ZPT_LOAD);
  gtk_list_store_set(store, &active_ld_iter,
		LD_FREQ, f,
		LD_REAL, r,
		LD_REACT, x,
		LD_FREQ_VAL, tload->f,
		LD_REAL_VAL, tload->r,
		LD_REACT_VAL, tload->x,
		LD_POINT, tload->pt,
		-1);

  enable_load_buttons(LOADLIST_UPDATED);
}


void
active_load_rx_update(void)
{
  if (active_ld_iter.stamp == 0) return;
  load_rx_valid(&active_load);
  load_rx_update(&active_load);
}


void
load_rx_append(load_definition *tload)
{
  GtkTreeIter iter;
  GtkTreeModel *ldstore;
  
  ldstore = gtk_tree_view_get_model(GTK_TREE_VIEW(ld_view));
  gtk_list_store_append(GTK_LIST_STORE(ldstore), &active_ld_iter);

  tload->pt = create_load_point();
  load_rx_update(tload);
}


void
active_load_rx_remove(void)
{
  GtkListStore *store;
  chart_point *pt;

  if (active_ld_iter.stamp == 0) return;

  store = GTK_LIST_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(ld_view)));
  gtk_tree_model_get(GTK_TREE_MODEL(store), &active_ld_iter,
		LD_POINT, &pt,
		-1);

  if (pt) {
    gtk_object_destroy(GTK_OBJECT(pt->point));
    g_free(pt);
  }
                              
  gtk_list_store_remove(store, &active_ld_iter);

  enable_load_rx_buttons(LOADLIST_REMOVED);
  if (number_loads() == 0)
    enable_load_rx_buttons(LOADLIST_EMPTY);
}


void
loadlist_rx_clear(void)
{
  enable_load_rx_buttons(LOADLIST_EMPTY);
}


void
load_rx_modified(int which)
{
  enable_load_buttons(LOADLIST_EDITED);
}

