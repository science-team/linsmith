/*
 *  load.c: Load related functions, list management etc.
 *
 *  Copyright (C) 1997-2005 John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <gtk/gtk.h>
#include <math.h>

#include "types.h"

enum {LD_FREQ, LD_REAL, LD_REACT,
      LD_FREQ_VAL, LD_REAL_VAL, LD_REACT_VAL,
      LD_NB_RVAL, LD_NB_CVAL, LD_NB_EXT,
      LD_POINT, LD_LOGCOL,
      LD_COLS};
      
enum {LOADLIST_SELECT, LOADLIST_UPDATED, LOADLIST_EDITED,
      LOADLIST_REMOVED, LOADLIST_EMPTY};
      
enum {LOADMODE_R_JX, LOADMODE_NOISE, LOADMODE_REMOTE};
      
void	initialize_loadtable(void);
void    get_load_selected(GtkTreeView *treeview);

int	load_valid(load_definition *dest);
void	load_append(load_definition *tload);
void    load_update(load_definition *tload);
void	load_modified(int which);
int	number_loads(void);
void	loadlist_clear();
void	active_load_update();
void	active_load_remove(void);

void	load_loads(char *fn);
void	save_loads(char *fn);

void	load4_activate(void);
void	save5_activate(void);
void	save_as3_activate(void);

