/*
 *  aux.c - auxiliary routines, mainly gp screen management
 *
 *  Copyright (C) 1997-2005 John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <gtk/gtk.h>
#include <math.h>
#include <libxml/tree.h>

//	Import/load value dialog:
enum   {LSTDLG_APPEND,		// append new values
	LSTDLG_CLEAR,		// clear list first
	LSTDLG_CANCEL};		// cancel import

int             ok_cancel_dialog(char *ttl, char *msg);
int             ok_dialog(char *ttl, char *msg);
int		list_not_empty_dialog(char *msg);
void            show_about(void);
gboolean	run_filedialog(char *title, char *fn, gboolean openfile, ...);

void		initialize_MainWindow(void);
int		xml_file_open(xmlDocPtr *doc, const char *fname, char *title);

void		set_element_conn(int val);
void		set_element_type(int val);
GdkPixbuf 	*connection_glyph(int conn, int type);
GdkPixbuf       *type_glyph(int conn, int type);

void		scale_entry_pair_set(int eltyp, double val);
void		scale_entry_pair_set_entry(int which, double val);
double		scale_entry_pair_get(int which);
