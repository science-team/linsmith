// cables.c
//
// Copyright (C) 2012 - Unknown
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "support.h"	// Needed for gettext macros
#include "main.h"
#include "cables.h"

typedef struct {
  char *name;
  double z0, vf, db1, mhz1, db2, mhz2;
} cabletable_entry;

cabletable_entry cabletable[] = {
  {N_("RG-5/U"),	52.5, 0.659,  0.77,  10.0,  2.90,  100.0},
  {N_("RG-5B/U"), 	50.0, 0.659,  0.66,  10.0,  2.40,  100.0},           
  {N_("RG-6A/U"), 	75.0, 0.659,  0.78,  10.0,  2.90,  100.0},           
  {N_("RG-6 Foam"),     75.0, 0.780,  5.30,  50.0, 16.20,  500.0},
  {N_("RG-8A/U"), 	50.0, 0.659,  0.55,  10.0,  2.00,  100.0},           
  {N_("RG-8 foam"),	50.0, 0.800,  1.70, 100.0,  6.00, 1000.0},
  {N_("RG-9/U"), 	51.0, 0.659,  0.57,  10.0,  2.00,  100.0},           
  {N_("RG-9B/U"), 	50.0, 0.659,  0.61,  10.0,  2.10,  100.0},           
  {N_("RG-10A/U"), 	50.0, 0.659,  0.55,  10.0,  2.00,  100.0},           
  {N_("RG-11A/U"), 	75.0, 0.660,  0.70,  10.0,  2.30,  100.0},           
  {N_("RG-11 foam"),	75.0, 0.780,  3.30,  50.0, 12.10,  500.0},
  {N_("RG-12A/U"), 	75.0, 0.659,  0.66,  10.0,  2.30,  100.0},           
  {N_("RG-13A/U"), 	75.0, 0.659,  0.66,  10.0,  2.30,  100.0},           
  {N_("RG-14A/U"), 	50.0, 0.659,  0.41,  10.0,  1.40,  100.0},           
  {N_("RG-16A/U"), 	52.0, 0.670,  0.40,  10.0,  1.20,  100.0},           
  {N_("RG-17A/U"), 	50.0, 0.659,  0.23,  10.0,  0.80,  100.0},           
  {N_("RG-18A/U"), 	50.0, 0.659,  0.23,  10.0,  0.80,  100.0},           
  {N_("RG-19A/U"), 	50.0, 0.659,  0.17,  10.0,  0.68,  100.0},           
  {N_("RG-20A/U"), 	50.0, 0.659,  0.17,  10.0,  0.68,  100.0},           
  {N_("RG-21A/U"), 	50.0, 0.659,  4.40,  10.0, 13.00,  100.0},           
  {N_("RG-29/U"), 	53.5, 0.659,  1.20,  10.0,  4.40,  100.0},           
  {N_("RG-34A/U"), 	75.0, 0.659,  0.29,  10.0,  1.30,  100.0},           
  {N_("RG-34B/U"), 	75.0, 0.660,  0.30,  10.0,  1.40,  100.0},           
  {N_("RG-35A/U"), 	75.0, 0.659,  0.24,  10.0,  0.85,  100.0},           
  {N_("RG-54A/U"), 	58.0, 0.659,  0.74,  10.0,  3.10,  100.0},           
  {N_("RG-55B/U"), 	53.5, 0.659,  1.30,  10.0,  4.80,  100.0},           
  {N_("RG-55A/U"), 	50.0, 0.659,  1.30,  10.0,  4.80,  100.0},           
  {N_("RG-58/U"), 	53.5, 0.660,  1.25,  10.0,  4.65,  100.0},           
  {N_("RG-58A/U"), 	53.5, 0.659,  1.25,  10.0,  4.65,  100.0},           
  {N_("RG-58C/U"), 	50.0, 0.659,  1.40,  10.0,  4.90,  100.0},           
  {N_("RG-58 foam"),	53.5, 0.790,  3.80, 100.0,  6.00,  300.0},
  {N_("RG-59A/U"), 	75.0, 0.659,  1.10,  10.0,  3.40,  100.0},           
  {N_("RG-59B/U"), 	75.0, 0.660,  1.10,  10.0,  3.40,  100.0},           
  {N_("RG-59 foam"),	75.0, 0.790,  3.80, 100.0,  6.00,  300.0},
  {N_("RG-62A/U"), 	93.0, 0.840,  0.85,  10.0,  2.70,  100.0},           
  {N_("RG-74A/U"), 	50.0, 0.659,  0.38,  10.0,  1.50,  100.0},           
  {N_("RG-83/U"), 	35.0, 0.660,  0.80,  10.0,  2.80,  100.0},           
  {N_("RG-174A/"), 	50.0, 0.660,  3.40,  10.0, 10.60,  100.0},           
  {N_("RG-213/U"), 	50.0, 0.660,  0.60,  10.0,  1.90,  100.0},           
  {N_("RG-218/U"), 	50.0, 0.660,  0.20,  10.0,  1.00,  100.0},           
  {N_("RG-220/U"), 	50.0, 0.660,  0.20,  10.0,  0.70,  100.0},
  {N_("UR-43"),		52.0, 0.660,  1.30,  10.0,  4.30,  100.0},
  {N_("UR-57"),		75.0, 0.660,  0.60,  10.0,  1.90,  100.0},
  {N_("UR-63"), 	75.0, 0.960,  0.15,  10.0,  0.50,  100.0},
  {N_("UR-67"), 	50.0, 0.660,  0.60,  10.0,  2.00,  100.0},
  {N_("UR-70"), 	75.0, 0.660,  1.50,  10.0,  4.90,  100.0},
  {N_("UR-74"), 	51.0, 0.660,  0.30,  10.0,  1.00,  100.0},
  {N_("UR-76"), 	51.0, 0.660,  1.60,  10.0,  5.30,  100.0},
  {N_("UR-77"), 	75.0, 0.660,  0.30,  10.0,  1.00,  100.0},
  {N_("UR-79"), 	50.0, 0.960,  0.16,  10.0,  0.50,  100.0},
  {N_("UR-83"), 	50.0, 0.960,  0.25,  10.0,  0.80,  100.0},
  {N_("UR-85"), 	75.0, 0.960,  0.20,  10.0,  0.70,  100.0},
  {N_("UR-90"), 	75.0, 0.660,  1.10,  10.0,  3.50,  100.0},
  {N_("UR-95"), 	50.0, 0.660,  2.60,  10.0,  8.20,  100.0},
  {N_("Belden 8240"),	50.0, 0.660,  4.90, 100.0, 20.00, 1000.0},
  {N_("Belden 8267"),   50.0, 0.660,  2.20, 100.0,  8.00, 1000.0},
  {N_("Belden 8208"),   50.0, 0.660,  0.00,   0.0,  8.00, 1000.0},
  {N_("Belden 9258"),   50.0, 0.780,  3.70, 100.0, 12.80, 1000.0},
  {N_("Belden 9880"),   50.0, 0.820,  1.30, 100.0,  4.50, 1000.0},
  {N_("Belden 9913"),   50.0, 0.820,  1.30, 100.0,  4.50, 1000.0},
  {N_("Belden 9914"),   50.0, 0.660,  0.00,   0.0,  9.00, 1000.0},
  {N_("3C-2V"),         75.0, 0.850,  0.40,   1.0, 20.00,  200.0},       // http://www.billowcable.com/sdp/430189/4/pd-2410269/3663633-1208049/3C-2V_Coaxial_Cable.html
  {N_("5C-2V"),         75.0, 0.850,  0.82,   1.0,  3.84,  200.0},       // http://www.billowcable.com/sdp/430189/4/pd-2410269/3663634-1208049/5C-2V_Coaxial_Cable.html
  {N_("LMR-100"),       50.0, 0.660,  5.10,  50.0, 24.00, 1000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=16346&eventPage=1
  {N_("LMR-195MA"),     50.0, 0.800,  2.60,  50.0, 11.77, 1000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=374248&eventGroup=4&eventPage=1
  {N_("LMR-195"),       50.0, 0.800,  2.60,  50.0, 11.77, 1000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=374248&eventGroup=4&eventPage=1
  {N_("LMR-195FR"),     50.0, 0.800,  2.50,  50.0, 11.30, 1000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=422765&eventGroup=4&eventPage=1
  {N_("LMR-195DB"),     50.0, 0.800,  2.50,  50.0, 11.30, 1000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=492041&eventGroup=4&eventPage=1
  {N_("LMR-200"),       50.0, 0.830,  2.50,  50.0, 10.44, 1000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=89695&eventGroup=4&eventPage=1
  {N_("LMR-200FR"),     50.0, 0.830,  2.50,  50.0, 10.44, 1000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=15236&eventGroup=4&eventPage=1
  {N_("LMR-200DB"),     50.0, 0.830,  2.50,  50.0, 10.44, 1000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=437471&eventGroup=4&eventPage=1
  {N_("LMR-200MA"),     50.0, 0.830,  2.50,  50.0, 10.44, 1000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=34936&eventGroup=4&eventPage=1
  {N_("LMR-600"),       50.0, 0.870,  0.55,  50.0,  3.90, 2000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=86672&eventGroup=4&eventPage=1
  {N_("LMR-600"),       50.0, 0.870,  0.55,  50.0,  3.90, 2000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=86672&eventGroup=4&eventPage=1
  {N_("LMR-600FR"),     50.0, 0.870,  0.55,  50.0,  3.90, 2000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=56313&eventGroup=4&eventPage=1
  {N_("LMR-600UF"),     50.0, 0.870,  0.66,  50.0,  3.90, 2000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=39425&eventGroup=4&eventPage=1
  {N_("LMR-600DB"),     50.0, 0.870,  0.55,  50.0,  3.90, 2000.0},       // http://www.tessco.com/products/displayProductInfo.do?sku=15605&eventGroup=4&eventPage=1
  {NULL}
};

char *stub_entries[] = {
  "comp_stub_vf_entry",
  "comp_stub_l1_entry",
  "comp_stub_f1_entry",
  "comp_stub_l2_entry",
  "comp_stub_f2_entry"
};

char *line_entries[] = {
  "comp_line_vf_entry",
  "comp_line_l1_entry",
  "comp_line_f1_entry",
  "comp_line_l2_entry",
  "comp_line_f2_entry"
};

typedef struct {
  char *title;
  int width;
} cable_col;

cable_col cablecol[] = {
  {N_("Cable type"),    120},
  {N_("Zo."),           40},
  {N_("Vf"),            48},
  {N_("dB @"),          50},
  {N_("MHz"),           60},
  {N_("dB @"),          50},
  {N_("MHz"),           60}
};
#define cablecol_len (sizeof(cablecol)/sizeof(cable_col))


void
load_cable_table(GtkWidget *ref)
{
  int i;
  char z0[10], vf[8], db1[10], db2[10], mhz1[12], mhz2[12];
  GtkListStore *cables;
  GtkTreeView *cbl_view;
  GtkTreeViewColumn *col;
  GtkTreeIter iter;
  GtkCellRenderer *renderer;
  
  cables = gtk_list_store_new(cablecol_len+1,
	G_TYPE_STRING,			// Cable name
	G_TYPE_STRING,			// z0
	G_TYPE_STRING,			// vf
	G_TYPE_STRING,			// db1
	G_TYPE_STRING,			// mhz1
	G_TYPE_STRING,			// db2
	G_TYPE_STRING,			// mhz2
	G_TYPE_INT);			// Nr in table

  cbl_view = GTK_TREE_VIEW(lookup_widget(ref, "cable_view"));
  gtk_tree_view_set_model(cbl_view,  GTK_TREE_MODEL(cables));
  g_object_unref(G_OBJECT(cables));

  for (i = 0; i < cablecol_len; i++) {
    renderer = gtk_cell_renderer_text_new();
    gtk_object_set(GTK_OBJECT(renderer),
		"height", 16,
		"xalign", (i==0 ? 0.0 : 1.0),
		NULL);
    col = gtk_tree_view_column_new_with_attributes(cablecol[i].title,
	renderer, "text", i, NULL);
    gtk_tree_view_append_column(cbl_view, col);
    gtk_tree_view_column_set_sizing(col, GTK_TREE_VIEW_COLUMN_FIXED);
    gtk_tree_view_column_set_fixed_width(col, cablecol[i].width);
  }

  for (i = 0; cabletable[i].name; i++) {
    gtk_list_store_append(cables, &iter);
    sprintf(z0, "%.1f", cabletable[i].z0);
    sprintf(vf, "%.3f", cabletable[i].vf);
    sprintf(db1, "%.2f", cabletable[i].db1);
    sprintf(mhz1, "%.0f", cabletable[i].mhz1);
    sprintf(db2, "%.2f", cabletable[i].db2);
    sprintf(mhz2, "%.0f", cabletable[i].mhz2);
    gtk_list_store_set(cables, &iter,
		CBLTBL_NAME, cabletable[i].name,
		CBLTBL_Z0, z0,
		CBLTBL_VF, vf,
		CBLTBL_DB1, db1,
		CBLTBL_MHZ1, mhz1,
		CBLTBL_DB2, db2,
		CBLTBL_MHZ2, mhz2,
		CBLTBL_I, i,
		-1);
  }
}

int
get_selected_cable_nr(GtkWidget *ref)
{
  GtkTreeView *vw;
  GtkTreeModel *model;
  GtkTreeSelection *sel;
  GtkTreeIter iter;
  int nr;
    
  vw = GTK_TREE_VIEW(lookup_widget(ref, "cable_view"));
  sel = gtk_tree_view_get_selection(vw);
  if (gtk_tree_selection_get_selected(sel, &model, &iter)) {
    gtk_tree_model_get(model, &iter,
                CBLTBL_I, &nr,
                -1);
    return nr;
  } else
    return -1;
}

void
show_cable_data(int apply_to, int cable_nr)
{
  GtkWidget *w;
  int d;
  char **entbl, *bff;
  
  switch (apply_to) {
    case SEP_STUB_VAL: 
      entbl = stub_entries;
      scale_entry_pair_set(SEP_STUB_Z0, cabletable[cable_nr].z0);
      break;
          
    case SEP_LINE_VAL:
      entbl = line_entries;
      scale_entry_pair_set(SEP_LINE_Z0, cabletable[cable_nr].z0);
      break;
  }
      
  for (d = 0; d < 5; d++) {
    switch (d) {
      case 0: bff = g_strdup_printf("%.3f", cabletable[cable_nr].vf);   break;
      case 1: bff = g_strdup_printf("%.2f", cabletable[cable_nr].db1);  break;
      case 2: bff = g_strdup_printf("%.0f", cabletable[cable_nr].mhz1); break;
      case 3: bff = g_strdup_printf("%.2f", cabletable[cable_nr].db2);  break;
      case 4: bff = g_strdup_printf("%.0f", cabletable[cable_nr].mhz2); break;
    }
    w = lookup_widget(MainWindow, entbl[d]);
    gtk_entry_set_text(GTK_ENTRY(w), bff);
    g_free(bff);
  }
}

/*
 *	These routines manage the component selection and connection
 *	type.
 */

/*	Load the element callbacks and set the button icon, from the
 *	table supplied.
 *	Used for the connection type and element buttons.
 */
