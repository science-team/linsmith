/*
 *  aux.c - auxiliary routines, mainly gp screen management
 *
 *  Copyright (C) 1997-2005 John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "support.h"
#include "callbacks.h"
#include "main.h"
#include "global.h"
#include "element.h"
#include "pixmaps.inc"
#include "printer.h"
#include "remote.h"
#include "misc.h"


char	*authors[] = {prog_author, NULL},
	run_bff[200];
static  int scale_timer_id = -1;

enum {ENTRY_VALID, ENTRY_INVALID, ENTRY_SET};

//	Scale/entry pair definitions

typedef struct {
  char *name, nrdec;
  double *valdst;
  GtkWidget *scale, *entry;
} scale_handler;

scale_handler sc_handler[] = {
  {"ind_val",	2, &act_el.val1},	// SEP_IND_VAL
  {"cap_val",   2, &act_el.val2},	// SEP_CAP_VAL
  {"stub",      1, &act_el.val1},	// SEP_STUB_VAL
  {"stub_z0",   1, &act_el.z0},		// SEP_STUB_Z0
  {"line",      1, &act_el.val1},	// SEP_LINE_VAL
  {"line_z0",   1, &act_el.z0},		// SEP_LINE_Z0
  {"xform",     2, &act_el.val1}	// SEP_XFORM_VAL
};
#define nr_sc_handlers (sizeof(sc_handler)/sizeof(scale_handler))

typedef struct {
  char *name;			// Shorthand name (will be amended)
  const guint8 *def;		// Pointer to the pixmap definition
  int size, 			// Size of the pixmap
      page, 			// Components: Definition page
      id;			// Temp: Callback id
  GtkWidget *wdg;		// Temp: The widget created
  GdkPixbuf *pxb;		// Temp: The pixbuf created
} toggle_button_def;


/*
 *	IMPORTANT: The definitions should be in the same order as the
 *	type definitions for connection type and element type!
 */

// Name        Icon definition    Icon def size

toggle_button_def conn_btn[] = {
  {"series",   comp_series_def,   sizeof(comp_series_def)},
  {"parallel", comp_parallel_def, sizeof(comp_parallel_def)},
  {"empty",    comp_empty_def,    sizeof(comp_empty_def)}
};
#define conn_btn_len (sizeof(conn_btn)/sizeof(toggle_button_def)-1)

// Name        Icon definition    Icon def size           page

toggle_button_def type_btn[] = {
  {"discr_c",     comp_cap_def,   sizeof(comp_cap_def),   0},
  {"discr_l",     comp_ind_def,   sizeof(comp_ind_def),   0},
  {"discr_parlc", comp_lcpar_def, sizeof(comp_lcpar_def), 0},
  {"discr_serlc", comp_lcser_def, sizeof(comp_lcser_def), 0},
  {"ostub",       comp_ostub_def, sizeof(comp_ostub_def), 1},
  {"sstub",       comp_sstub_def, sizeof(comp_sstub_def), 1},
  {"line",        comp_line_def,  sizeof(comp_line_def),  2},
  {"xform",       comp_xform_def, sizeof(comp_xform_def), 3},
  {"zy",          comp_zy_def,    sizeof(comp_zy_def),    4},
  {"empty",       comp_empty_def, sizeof(comp_empty_def), 9}
};
#define type_btn_len (sizeof(type_btn)/sizeof(toggle_button_def)-1)


int
ok_cancel_dialog(char *ttl, char *msg)
{
  GtkWidget *dlg, *lbl;

  dlg = gnome_dialog_new(ttl, _("Ok"), _("Cancel"), NULL);
  gtk_window_set_position(GTK_WINDOW(dlg), GTK_WIN_POS_CENTER);
  lbl = gtk_label_new(msg);
  gtk_widget_show(lbl);
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dlg)->vbox),
                     lbl, TRUE, TRUE, 0);
  return gnome_dialog_run_and_close(GNOME_DIALOG(dlg));
}


int
ok_dialog(char *ttl, char *msg)
{
  GtkWidget *dlg, *lbl;

  dlg = gnome_dialog_new(ttl, _("Ok"), NULL);
  gtk_window_set_position(GTK_WINDOW(dlg), GTK_WIN_POS_CENTER);
  lbl = gtk_label_new(msg);
  gtk_widget_show(lbl);
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dlg)->vbox),
                     lbl, TRUE, TRUE, 0);
  return gnome_dialog_run_and_close(GNOME_DIALOG(dlg));
}


//	Confirmation dialog in case the element/load list is not empty

int
list_not_empty_dialog(char *msg)
{
  GtkWidget *dlg, *lbl;

  dlg = gnome_dialog_new(
		_("List is not empty"),
		("_Append"), _("Clear first"), _("Cancel"),
		NULL);
  gtk_window_set_position(GTK_WINDOW(dlg), GTK_WIN_POS_CENTER);
  lbl = gtk_label_new(msg);
  gtk_widget_show(lbl);
  gtk_box_pack_start(GTK_BOX(GNOME_DIALOG(dlg)->vbox),
		lbl, TRUE, TRUE, 0);
  return gnome_dialog_run_and_close(GNOME_DIALOG(dlg));
}

gboolean
run_filedialog1(char *title, char *fn, char *filter)
{
  GtkWidget *fs, *selbtn;
  int result;

  fs = gtk_file_selection_new(title);
  selbtn = GTK_FILE_SELECTION(fs)->ok_button;

  gtk_file_selection_set_filename(GTK_FILE_SELECTION(fs), fn);
  gtk_window_set_modal(GTK_WINDOW(fs), TRUE);

  result = gtk_dialog_run(GTK_DIALOG(fs));

  if (result == GTK_RESPONSE_OK)
    strcpy(fn, gtk_file_selection_get_filename(GTK_FILE_SELECTION(fs)));

  gtk_widget_destroy(fs);

  return (result == GTK_RESPONSE_OK);
}


gboolean
run_filedialog(char *title, char *fn, gboolean openfile, ...)
{
  va_list parg;
  GtkWidget *fc;
  GtkFileFilter *ff;
  int result;
  char *p, *pname, *pfilt;

  va_start(parg, openfile);
  if (openfile) {
    fc = gtk_file_chooser_dialog_new(title, NULL, 
		GTK_FILE_CHOOSER_ACTION_OPEN,
		GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
		GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
		NULL);
  } else {
    fc = gtk_file_chooser_dialog_new(title, NULL, 
		GTK_FILE_CHOOSER_ACTION_SAVE,
		GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
		GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT,
		NULL);
  }

  while ((pname = va_arg(parg, char *)) != NULL) {
    pfilt = va_arg(parg, char *);
    ff = gtk_file_filter_new();
    gtk_file_filter_set_name(ff, pname);
    gtk_file_filter_add_pattern(ff, pfilt);
    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(fc), ff);
  }
  va_end(parg);

  result = gtk_dialog_run(GTK_DIALOG(fc));
  if (result == GTK_RESPONSE_ACCEPT) {
    strcpy(fn, p = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(fc)));
    g_free(p);
  }

  gtk_widget_destroy(fc);

  return (result == GTK_RESPONSE_ACCEPT);
}


void
show_about(void)
{
  GdkPixbuf *pxb = gdk_pixbuf_new_from_inline(sizeof(logo_def),
                logo_def, FALSE, NULL);
  GtkWidget *w = gnome_about_new(prog_name, prog_version, prog_right,
                                 prog_com, (const gchar **)authors,
                                 NULL, NULL, pxb);

  gtk_widget_show(w);
}


int
xml_file_loaded(xmlDocPtr *doc, const char *fname, char *title)
{
  xmlNodePtr cur;

  *doc = xmlParseFile(fname);
  if (*doc == NULL ) {
    ok_dialog(_("XML Error"), 
	      _("Document not parsed successfully."));
    return FALSE;
  }
	
  cur = xmlDocGetRootElement(*doc);
	
  if (cur == NULL) {
    ok_dialog(_("XML Error"), _("Empty document"));
    xmlFreeDoc(*doc);
    return FALSE;
  }
	
  if (xmlStrcmp(cur->name, (const xmlChar *) title)) {
    ok_dialog(_("XML Error"), _("Document of the wrong type."));
    xmlFreeDoc(*doc);
    return FALSE;
  }

  return TRUE;
}


void
load_element_toggles(toggle_button_def *tbl, int len, GCallback cb)
{
  GtkWidget *tb_img;
  char bff[30];
  
  int c;
  
  for (c = 0; c < len; c++) {
    sprintf(bff, "comp_%s_img", tbl[c].name);
    tb_img = lookup_widget(MainWindow, bff);
    sprintf(bff, "comp_%s_tbtn", tbl[c].name);
    tbl[c].wdg = lookup_widget(MainWindow, bff);

    tbl[c].id = g_signal_connect(G_OBJECT(tbl[c].wdg), "clicked", cb,
                GINT_TO_POINTER(c));

    tbl[c].pxb = gdk_pixbuf_new_from_inline(tbl[c].size, tbl[c].def, 
                FALSE, NULL);
    gtk_image_set_from_pixbuf(GTK_IMAGE(tb_img), tbl[c].pxb);
  }
}

/*	Make the possible elements visible according to the connection
 *	selected (series/parallel)
 */

void
element_show_possible(int conntype)
{
  GtkWidget *w;

  w = lookup_widget(MainWindow, "comp_line_tbtn");
  g_object_set(G_OBJECT(w), "visible", act_el.conn == ELC_SERIES, NULL);
  w = lookup_widget(MainWindow, "comp_xform_tbtn");
  g_object_set(G_OBJECT(w), "visible", act_el.conn == ELC_SERIES, NULL);
}

/*	Set the buttons from the selected element. Common routine to
 *	set the connection type and element type buttons.
 *	Have to disable the callbacks temporarily here!
 */

void
set_element_toggles(toggle_button_def *tbl, int len, int val)
{
  int c;
  
  for (c = 0; c < len; c++) {
    g_signal_handler_block(G_OBJECT(tbl[c].wdg), tbl[c].id);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(tbl[c].wdg), val == c);
    g_signal_handler_unblock(G_OBJECT(tbl[c].wdg), tbl[c].id);
  }
}

/*	Set the connection type (series/parallel)
 */

void
set_element_conn(int type)
{
  GtkWidget *w;
  
  act_el.conn = type;  
  set_element_toggles(conn_btn, conn_btn_len, type);
  element_show_possible(type);
}

/*	Set the element type
 */

void
set_element_type(int type)
{
  GtkWidget *w = lookup_widget(MainWindow, "comp_notebook");

  set_element_toggles(type_btn, type_btn_len, type);
  act_el.typ = type;
  element_show_possible(type);
  set_discr_mode(type);
  gtk_notebook_set_current_page(GTK_NOTEBOOK(w), type_btn[type].page);
}


GdkPixbuf *
connection_glyph(int conn, int type)
{
  if (type == ELT_ZY)
    return conn_btn[ELC_EMPTY].pxb;
  else
    return conn_btn[conn].pxb;
}

GdkPixbuf *
type_glyph(int conn, int type)
{
  return type_btn[type].pxb;
}

//---------------------------------------------------------------------
//	Scale/entry pair handling
//---------------------------------------------------------------------


void
set_buttons(int which, int state)
{
  GtkWidget *w;

//  gtk_widget_set_sensitive(sc_handler[which].scale,
//    state == ENTRY_VALID || state == ENTRY_SET);
}


void
scale_entry_pair_set_scale(int eltyp, double val)
{
  GtkRange *range = GTK_RANGE(sc_handler[eltyp].scale);
  int id;

  id = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(range), "chg_id"));
  g_signal_handler_block(G_OBJECT(range), id);
  gtk_range_set_range(range, log(val/2), log(val*2));
  gtk_range_set_value(range, log(val));
  gtk_widget_set_sensitive(GTK_WIDGET(range), TRUE);
  g_signal_handler_unblock(G_OBJECT(range), id);
}


void
scale_entry_pair_set_entry(int which, double val)
{
  GtkEntry *entry = GTK_ENTRY(sc_handler[which].entry);
  char bff[20];
  int id;

  *sc_handler[which].valdst = val;

  sprintf(bff, "%.*f", sc_handler[which].nrdec, val);
  id = GPOINTER_TO_INT(g_object_get_data(G_OBJECT(entry), "chg_id"));
  g_signal_handler_block(G_OBJECT(entry), id);
  gtk_entry_set_text(GTK_ENTRY(sc_handler[which].entry), bff);
  g_signal_handler_unblock(G_OBJECT(entry), id);

  enable_circ_buttons(CIRCLIST_ENTRYSET);
}


void
scale_entry_pair_set(int eltyp, double val)
{
  scale_entry_pair_set_scale(eltyp, val);
  scale_entry_pair_set_entry(eltyp, val);
}


double
scale_entry_pair_get(int which)
{
  return atof(gtk_entry_get_text(GTK_ENTRY(sc_handler[which].entry)));
}

//
//	Range ('scale') changes:
//

void
on_comp_scale_value_changed(GtkRange *range, gpointer user_data)
{
  double val = exp(gtk_range_get_value(range));

  scale_entry_pair_set_entry(GPOINTER_TO_INT(user_data), val);
  recalculate_all(DST_SCREEN);
}

gboolean
on_comp_scale_button_pressed(GtkWidget *wdg, GdkEventButton *event,
				gpointer user_data)
{
  if (scale_timer_id != -1)
    gtk_timeout_remove(scale_timer_id);
  return FALSE;
}

gboolean
on_scale_timeout(gpointer user_data)
{
  GtkEntry *entry = GTK_ENTRY(sc_handler[GPOINTER_TO_INT(user_data)].entry);
  scale_entry_pair_set_scale(GPOINTER_TO_INT(user_data),
                        atof(gtk_entry_get_text(entry)));
  scale_timer_id = -1;
  return FALSE;
}

gboolean
on_comp_scale_button_release(GtkWidget *wdg, GdkEventButton *event,
				gpointer user_data)
{
  scale_timer_id = gtk_timeout_add(1000, on_scale_timeout, user_data);
  return FALSE;
}

//
//	Value 'Entry' events;
//
// 	A new value has been entered in the component entry box
//	update the scale, and the component list if
//	the component was already added.

void
on_comp_entry_activation(GtkEntry *entry, gpointer user_data)
{
  scale_entry_pair_set_scale(GPOINTER_TO_INT(user_data),
			atof(gtk_entry_get_text(entry)));
  gtk_widget_grab_focus(GTK_WIDGET(entry));
  if (element_valid(&act_el)) element_update(act_el);
}

void
on_comp_entry_changed(GtkEntry *entry, gpointer user_data)
{
  char *pend;
  const char *p = gtk_entry_get_text(entry);
  double val;
  int which = GPOINTER_TO_INT(user_data);

  val = strtod(p, &pend);
  if (p != pend && val != 0) {			// Then it's a valid entry
    scale_entry_pair_set_scale(which, val);
    enable_circ_buttons(CIRCLIST_ENTRYVALID);
    *sc_handler[which].valdst = val;
  } else {					// Not a valid entry
    enable_circ_buttons(CIRCLIST_ENTRYINVALID);
  }
}

void
on_comp_entry_focus_out(GtkEntry *entry, GdkEventFocus *event,
			gpointer user_data)
{
  scale_entry_pair_set_scale(GPOINTER_TO_INT(user_data),
                        atof(gtk_entry_get_text(entry)));
}

//
//	Set all the handlers
//

void
enable_scale_handlers(void)
{
  GtkWidget *w, *e;
  char bff[30];
  int s, id;

  for (s = 0; s < nr_sc_handlers; s++) {
    sprintf(bff, "comp_%s_scale", sc_handler[s].name);
    sc_handler[s].scale = lookup_widget(MainWindow, bff);
    sprintf(bff, "comp_%s_entry", sc_handler[s].name);
    sc_handler[s].entry = lookup_widget(MainWindow, bff);

    // Define range event handlers
    id = g_signal_connect(G_OBJECT(sc_handler[s].scale),
		"value_changed",
		G_CALLBACK(on_comp_scale_value_changed),
		GINT_TO_POINTER(s));
    g_object_set_data(G_OBJECT(sc_handler[s].scale), "chg_id",
    		GINT_TO_POINTER(id));

    id = g_signal_connect(G_OBJECT(sc_handler[s].scale),
		"button_press_event",
		G_CALLBACK(on_comp_scale_button_pressed),
		GINT_TO_POINTER(s));

    id = g_signal_connect(G_OBJECT(sc_handler[s].scale),
		"button_release_event",
		G_CALLBACK(on_comp_scale_button_release),
		GINT_TO_POINTER(s));

    // Define entry event handlers
    id = g_signal_connect(G_OBJECT(sc_handler[s].entry),
		"activate",
		G_CALLBACK(on_comp_entry_activation),
		GINT_TO_POINTER(s));
    g_object_set_data(G_OBJECT(sc_handler[s].entry), "act_id",
    		GINT_TO_POINTER(id));
    g_object_set_data(G_OBJECT(sc_handler[s].entry), "id",
                GINT_TO_POINTER(s));                     

    id = g_signal_connect(G_OBJECT(sc_handler[s].entry),
                "changed",
                G_CALLBACK(on_comp_entry_changed),
                GINT_TO_POINTER(s));
    g_object_set_data(G_OBJECT(sc_handler[s].entry), "chg_id",
                GINT_TO_POINTER(id));                         
  }
}

//
//	Component type and connection button callbacks
//

void
on_comp_conn_tbtn_clicked(GtkButton *button, gpointer user_data)
{
  set_element_conn(GPOINTER_TO_INT(user_data));
  enable_circ_buttons(CIRCLIST_CONNCHANGED);
}


void
on_comp_type_tbtn_clicked (GtkButton *button, gpointer user_data)
{
  set_element_type(GPOINTER_TO_INT(user_data));
  enable_circ_buttons(CIRCLIST_TYPECHANGED);
}


//---------------------------------------------------------------------
//      Main window
//---------------------------------------------------------------------

void
initialize_MainWindow(void)
{
  char *ttl = g_strconcat(_(prog_title), " - v", prog_version, NULL);
  GtkWidget *w = lookup_widget(MainWindow, "z0_cbbox");
  GtkWidget *w1 = lookup_widget(MainWindow, "circpage_vbox");

  load_config();
  remote_create_fifos();

  pref.chart_z0 = 50.0;
  load_element_toggles(conn_btn, conn_btn_len, 
  			G_CALLBACK(on_comp_conn_tbtn_clicked));
  load_element_toggles(type_btn, type_btn_len, 
  			G_CALLBACK(on_comp_type_tbtn_clicked));
  set_element_conn(ELC_SERIES);
  set_element_type(ELT_CAP);
  enable_scale_handlers();

  gtk_window_set_title(GTK_WINDOW(MainWindow), ttl);
  g_free(ttl);

}
