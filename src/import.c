#include <gtk/gtk.h>
#include <math.h>

#include "interface.h"
#include "support.h"
#include "global.h"
#include "misc.h"
#include "main.h"
#include "import.h"

#define max_line_len	250

s2p_dataset	s2p_data;

GtkTextView	*imp_text = NULL;
GtkTextBuffer	*imp_bff = NULL;
GtkTreeView	*imp_view = NULL;
GtkListStore	*imp_list = NULL;
GtkTreeSelection *imp_sel = NULL;

enum {	S2P_DATA_FREQ,
	S2P_DATA_11r,
	S2P_DATA_11i,
	S2P_DATA_12r,
	S2P_DATA_12i,
	S2P_DATA_21r,
	S2P_DATA_21i,
	S2P_DATA_22r,
	S2P_DATA_22i,
	S2P_DATA_COLS
};

enum {	DEC_OK,
	DEC_END,
	DEC_ERR
};

enum {	S2P_OPT_R,
	S2P_OPT_GHz, S2P_OPT_MHz, S2P_OPT_kHz, S2P_OPT_Hz,
	S2P_OPT_S, S2P_OPT_Y, S2P_OPT_Z, S2P_OPT_H, S2P_OPT_G,
	S2P_OPT_dB, S2P_OPT_MA, S2P_OPT_RI
};

char *s2p_options[] = {
	"R",
	"GHz", "MHz", "kHz", "Hz",
	"S", "Y", "Z", "H", "G",
	"dB", "MA", "RI"
};
#define nr_s2p_options (sizeof(s2p_options)/sizeof(char *))

char *s2p_directives[] = {
	"Version",
	"Reference"
};
#define nr_s2p_directives (sizeof(s2p_directives)/sizeof(char *))

int	s2p_import_col;
double	s2p_prev_freq;

/*--------------------------------------------------------------------
	Touchstone file import
--------------------------------------------------------------------*/

/*	GUI related functions	*/

void
s2p_files1_activate(void)
{
  GtkWidget *w;

  w = create_Imports2pWindow();
  if (run_filedialog("Import file", pref.last_s2p_file, TRUE,
	_("s2p files"), "*.s2p", _("All files"), "*", NULL)) {
    w = create_Imports2pWindow();
    gtk_widget_show_all(w);
    if (!s2p_file_loaded(pref.last_s2p_file)) {
      gtk_widget_destroy(w);
      ok_dialog(_("Warning"), _("Cannot import the s2p file"));
    } else
      save_file_paths();
  }
}


void
Imports2pWindow_realize(GtkWidget *widget)
{
  GtkCellRenderer *renderer;
  GtkTreeViewColumn *col;
  int colnr;

  ImportWindow = widget;

  imp_text = GTK_TEXT_VIEW(lookup_widget(ImportWindow, "import_s2p_textview"));
  imp_bff = gtk_text_buffer_new(NULL);
  gtk_text_view_set_buffer(imp_text, imp_bff);

  imp_view = GTK_TREE_VIEW(lookup_widget(ImportWindow, "import_s2p_treeview"));
  imp_list = gtk_list_store_new(S2P_DATA_COLS,
		G_TYPE_STRING,		/* Frequency */
		G_TYPE_STRING,		/* real/mag 1 */
		G_TYPE_STRING,		/* imag/angle 1 */
		G_TYPE_STRING,		/* real/mag 2 */
		G_TYPE_STRING,		/* imag/angle 2 */
		G_TYPE_STRING,		/* real/mag 3 */
		G_TYPE_STRING,		/* imag/angle 3 */
		G_TYPE_STRING,		/* real/mag 4 */
		G_TYPE_STRING);		/* imag/angle 4 */

  gtk_tree_view_set_model(GTK_TREE_VIEW(imp_view), GTK_TREE_MODEL(imp_list));
  g_object_unref(G_OBJECT(imp_list));

  imp_sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(imp_view));
  gtk_tree_selection_set_mode(imp_sel, GTK_SELECTION_MULTIPLE);

  renderer = gtk_cell_renderer_text_new();
  col = gtk_tree_view_column_new_with_attributes(
		_("Freq"),
		renderer, "text", 0,
		NULL);
  gtk_tree_view_append_column(GTK_TREE_VIEW(imp_view), col);

  for (colnr = 1; colnr < 9; colnr++) {
    renderer = gtk_cell_renderer_text_new();
    gtk_object_set(GTK_OBJECT(renderer),
		"height", 14, "ypad", 0,
		"xalign", 1.0, NULL);
    col = gtk_tree_view_column_new_with_attributes(
		"",				/* Fill in the header later */
		renderer, "text", colnr,
		NULL);
    gtk_tree_view_append_column(GTK_TREE_VIEW(imp_view), col);
  }		
}

static gboolean
invert_selection_foreach (GtkTreeModel *model,
			  GtkTreePath *path,
			  GtkTreeIter *iter,
			  GtkTreeSelection *sel)
{
  if (gtk_tree_selection_iter_is_selected(sel, iter)) {
    gtk_tree_selection_unselect_iter(sel, iter);
  } else {
    gtk_tree_selection_select_iter (sel, iter);
  }
  return FALSE;
}


void
import_s2p_sel_invert_btn_clicked(void)
{
  gtk_tree_model_foreach(GTK_TREE_MODEL(imp_list),
	(GtkTreeModelForeachFunc) invert_selection_foreach,
	imp_sel);
}

void
import_s2p_sel_none_btn_clicked(void)
{
  gtk_tree_selection_unselect_all(imp_sel);
}

void
import_s2p_sel_all_btn_clicked(void)
{
  gtk_tree_selection_select_all(imp_sel);
}


char *s2p_col_headers[][8] = {
  {"dB|$<sub>11</sub>|", "&lt;$<sub>11</sub>",    
   "dB|$<sub>21</sub>|", "&lt;$<sub>21</sub>", 
   "dB|$<sub>12</sub>|", "&lt;$<sub>12</sub>",    
   "dB|$<sub>22</sub>|", "&lt;$<sub>22</sub>"},
  {"|$<sub>11</sub>|", "&lt;$<sub>11</sub>",
   "|$<sub>21</sub>|", "&lt;$<sub>21</sub>", 
   "|$<sub>12</sub>|", "&lt;$<sub>12</sub>",
   "|$<sub>22</sub>|", "&lt;$<sub>22</sub>"},
  {"Re[$<sub>11</sub>]", "Im[$<sub>11</sub>]", 
   "Re[$<sub>21</sub>]", "Im[$<sub>21</sub>]", 
   "Re[$<sub>12</sub>]", "Im[$<sub>12</sub>]", 
   "Re[$<sub>22</sub>]", "Im[$<sub>22</sub>]"}
};

void
s2p_set_col_headers(void)
{
  GtkTreeViewColumn *col;
  GtkWidget *lbl;
  int colnr, pos, rd;
  char p[20], *p1;

  for (colnr = 1; colnr < 9; colnr++) {
    strcpy(p, s2p_col_headers[s2p_data.fmt][colnr-1]);
    pos = strchr(p, '$') - p;
    p[pos] = s2p_options[s2p_data.param][0];
    col = gtk_tree_view_get_column(imp_view, colnr);
    lbl = gtk_label_new("");
    gtk_label_set_markup(GTK_LABEL(lbl), 
	g_locale_to_utf8(p, -1, NULL, NULL, NULL));
    gtk_widget_show(lbl);
    gtk_tree_view_column_set_widget(col, lbl);
  }
}


void
import_s2p_append_comment(char *line)
{
  GtkTextIter iter;

  gtk_text_buffer_get_end_iter(imp_bff, &iter);
  gtk_text_buffer_insert(imp_bff, &iter, "\n", -1);
  gtk_text_buffer_get_end_iter(imp_bff, &iter);
  gtk_text_buffer_insert(imp_bff, &iter, line, -1);
}

/*---------------------------------------------------------------------
	Touchstone format parser
---------------------------------------------------------------------*/

void
s2p_default_options(void)
{
  s2p_data.unit    = S2P_OPT_GHz;
  s2p_data.param   = S2P_OPT_S;
  s2p_data.fmt     = S2P_OPT_MA;
  s2p_data.z0      = 50.0;
  s2p_data.version = 1.0;
}

/*	Utility function to show an options string		*/
/*	Parameters: wdg: The name of the GtkLabel to set,	*/
/*	opt: the number of the text to set (see enum above	*/

void
s2p_show_option(char *wdg, int opt) 
{
  GtkWidget *w;
  char *p;

  w = lookup_widget(ImportWindow, wdg);
  p = g_strdup_printf("<b>%s</b>", s2p_options[opt]);
  gtk_label_set_markup(GTK_LABEL(w), p);
  g_free(p);
}

void
s2p_show_options(void) {
  GtkWidget *w;
  char *p;

  s2p_show_option("import_s2p_freq_lbl", S2P_OPT_GHz + s2p_data.unit);
  s2p_show_option("import_s2p_param_lbl", S2P_OPT_S + s2p_data.param);
  s2p_show_option("import_s2p_format_lbl", S2P_OPT_dB + s2p_data.fmt);

  w = lookup_widget(ImportWindow, "import_s2p_imped_lbl");
  p = g_strdup_printf("<b>%.1f</b>", s2p_data.z0);
  gtk_label_set_markup(GTK_LABEL(w), p);
  g_free(p);

  w = lookup_widget(ImportWindow, "import_s2p_version_lbl");
  p = g_strdup_printf("<b>%.1f</b>", s2p_data.version);
  gtk_label_set_markup(GTK_LABEL(w), p);
  g_free(p);
}

/*	#
	freq	GHZ MHZ KHZ HZ
	param	S Y Z G H
	format	DB MA RI
	R
	n	(real ohms)
*/

gboolean
s2p_options_decoded(char *line)
{
  GtkWidget *w;
  gboolean in_str, next_z0;
  char str[10][20], *p, *endp;
  int nr, c, dstp, o;

  nr = sscanf(line, "%s %s %s %s %s %s", 
	str[0], str[1], str[2], str[3], str[4], str[5]);

  /* Start at element 1 (el 0 is the # sign	*/
  for (c = 1; c < nr; c++) {
    for (o = 0; o < nr_s2p_options; o++)
      if (strcasecmp(s2p_options[o], str[c]) == 0) break;
    switch (o) {
      case 0: 						/* Zo */
        c++;
        if (c == nr) {
          ok_dialog(_("Error"), _("Impedance missing"));
	  return FALSE;
        }
        s2p_data.z0 = strtod(str[c], &endp);
        if (endp == str[c]) {
          ok_dialog(_("Error"), _("Error in impedance value"));
          return FALSE;
        }
	break;
      case 1: case 2: case 3: case 4:
	s2p_data.unit = o - S2P_OPT_GHz; break;		/* Unit */
      case 5: case 6: case 7: case 8: case 9: 
	s2p_data.param = o - S2P_OPT_S; break;		/* Param */
      case 10: case 11: case 12:
	s2p_data.fmt = o - S2P_OPT_dB; break;		/* Format */
      default:
        p = g_strdup_printf(_("Error in option line: %s"), str[c]);
	ok_dialog(_("Error"), p);
        g_free(p);
	return FALSE;
    }
  }

  return TRUE;
}

int
s2p_data_decoded(char *line)
{
  GtkTreeIter iter;
  char data[9][15], *endp;
  double dummy;
  int c, nr_fld;

  nr_fld = sscanf(line, "%14s %14s %14s %14s %14s %14s %14s %14s %14s",
	data[0],				/* Frequency */
	data[1], data[2], data[3], data[4],	/* Data fields */
	data[5], data[6], data[7], data[8]);

  dummy = strtod(data[0], &endp);
  if (dummy < s2p_prev_freq)			/* Check if end of data */
    return DEC_END;
  s2p_prev_freq = dummy;

  if (nr_fld != 9) {
    ok_dialog(_("Error"), _("Incorrect number of fields in data line"));
    return DEC_ERR;
  }
  for (c = 0; c < 9; c++) {
    dummy = strtod(data[c], &endp);
    if (endp == data[c]) {
      ok_dialog(_("Error"), _("Invalid data present on the next data line"));
      return DEC_ERR;
    }
  }
  gtk_list_store_append(imp_list, &iter);
  gtk_list_store_set(imp_list, &iter,
	0, data[0], 1, data[1], 2, data[2],
	3, data[3], 4, data[4], 5, data[5],
	6, data[6], 7, data[7], 8, data[8],
	-1);
  return DEC_OK;
}


gboolean
s2p_directive_decoded(char *line)
{
  int c;
  char dir[40], *endp;

  endp = strchr(line, ']');
  if (!endp) return FALSE;
  endp[0] = '\0';			/* Close the string here */

  for (c = 0; c < nr_s2p_directives; c++) {
    if (strcasecmp(s2p_directives[c], (char *) &line[1]) == 0) 
      break;
  }
  if (c >= nr_s2p_directives) return FALSE;

  return TRUE;
}


gboolean
s2p_file_loaded(char *filename)
{
  FILE *inf = fopen(filename, "r");
  gboolean done;
  GtkWidget *w;
  char line[max_line_len], *p;

  if (!inf) {
    return FALSE;
  }
  s2p_default_options();
  s2p_prev_freq = 0.0;			/* To detect noise parameters */
  done = FALSE;
  while (!feof(inf) && !done) {
    if (!fgets(line, max_line_len, inf))
      break;
    p = strchr(line, '\n');
    if (p) *p = 0;
    p = strchr(line, '\r');
    if (p) *p = 0;
    if (strlen(line) == 0)
      continue;
    switch (line[0]) {
      case '!': 			/* Comment line */
	import_s2p_append_comment(line);
	break;
      case '#': 			/* Config line */
	if (!s2p_options_decoded(line)) 
          return FALSE;
	s2p_set_col_headers();
        s2p_show_options();
	break;
      case '[':
        if (!s2p_directive_decoded(line))
          return FALSE;
        s2p_show_options();
        break;
      default: 				/* Data lines */
	switch (s2p_data_decoded(line)) {
          case DEC_OK:
	    break;
	  case DEC_END:
	    done = TRUE;
            break;
	  case DEC_ERR:
            return FALSE;
        }
    }
  }

  w = lookup_widget(ImportWindow, "import_s2p_impS11_btn");
  gtk_widget_set_sensitive(w, TRUE);
  w = lookup_widget(ImportWindow, "import_s2p_impS22_btn");
  gtk_widget_set_sensitive(w, TRUE);

  return TRUE;
}


void
import_s2p_cancel_btn_clicked(void)
{
  gtk_widget_destroy(ImportWindow);
}


static gboolean
s2p_import_one_selected(GtkTreeModel *model, GtkTreePath *path,
		  GtkTreeIter *iter, GtkTreeSelection *sel)
{
  char *str_a, *str_b, *str_f;
  load_definition load;
  double f, r, x, tr, tx;
  complex z;

  gtk_tree_model_get(GTK_TREE_MODEL(imp_list), iter,
	0, &str_f,
	s2p_import_col, &str_a,
	s2p_import_col+1, &str_b,
	-1);

  switch (s2p_data.unit) {
    case 0: load.f = strtod(str_f, NULL) * 1000; break;
    case 1: break;
    case 2: load.f = strtod(str_f, NULL) * 0.001; break;
    case 3: load.f = strtod(str_f, NULL) * 0.000001; break;
  }
  r = strtod(str_a, NULL);
  x = strtod(str_b, NULL);

  /* Convert S-parameters to a common (RI) format */
  switch (s2p_data.fmt) {
    case 0:	/* dB */
      r = pow(10, r/20);
      tr = r * cos(x * M_PI/180);
      tx = r * sin(x * M_PI/180);
      r = tr;
      x = tx;
      break;
    case 1:	/* MA */
      tr = r * cos(x * M_PI/180);
      tx = r * sin(x * M_PI/180);
      r = tr;
      x = tx;
      break;
    case 2:	/* RI */
      break;
  }

  z = s2p_data.z0 * (1.0 + (r + x*I))/(1.0 - (r + x*I));
  load.r = creal(z);
  load.x = cimag(z);
  load_append(&load);
  printf("%f %f %f\n", load.f, load.r, load.x);

  return FALSE;
}


void
s2p_import_all_selected(int col)
{
  s2p_import_col = col;
  gtk_tree_selection_selected_foreach(imp_sel,
	(GtkTreeSelectionForeachFunc) s2p_import_one_selected, imp_sel);
}

void
import_s2p_impS11_btn_clicked(void)
{
  s2p_import_all_selected(1);		/* import column 1 and 2 (s11) */
  gtk_widget_destroy(ImportWindow);
}


void
import_s2p_impS22_btn_clicked(void)
{
  s2p_import_all_selected(7);		/* importa column 7 and 8 (s22) */
  gtk_widget_destroy(ImportWindow);
}


/*
	CSV file import
*/

void
import_csv_activate(void)
{
  FILE *csvf = NULL;
  char line[max_line_len], *p, delim[5];
  load_definition load;

  if (number_loads() != 0) {
    switch (list_not_empty_dialog(
	_("The load list is not empty. Selecting Append will append\n"
	  "the new values."))) {
      case LSTDLG_APPEND: break;
      case LSTDLG_CLEAR : loadlist_clear(); break;
      case LSTDLG_CANCEL: return;
    }
  }

  if (strlen(pref.csv_sep) != 1) {
    ok_dialog(_("Error"),
              _("The field separator for CSV files is not defined yet."
	        "Please go\nto Edit|Preferences (Files tab) and define "
		"it first."));
    return;
  }

  if (!run_filedialog(_("Import CSV load file"), 
		pref.last_csv_file, TRUE,
		_("CSV files"), "*.csv",
                _("All files"), "*",                                            
                NULL)) 
    return;

  csvf = fopen(pref.last_csv_file, "r");
  if (!csvf) {
    ok_dialog(_("Error"),
	      _("Can't open this file (maybe read-protected?)"));
    return;
  }
  save_file_paths();

  strcpy(delim, " \n");
  delim[0] = pref.csv_sep[0];
  while (!feof(csvf)) {
    fgets(line, max_line_len, csvf);
    p = line;
    p = strtok(p, delim);
    if (!p) break;		/* No frequency token */
    load.f = strtod(p, NULL);

    p = strtok(NULL, delim);
    if (!p) break;		/* No real part available */
    load.r = strtod(p, NULL);

    p = strtok(NULL, delim);
    if (!p) break;		/* No imaginary part available */
    load.x = strtod(p, NULL);

    load_append(&load);
    printf("%f %f %f\n", load.f, load.r, load.x);
  }
  fclose(csvf);
}
