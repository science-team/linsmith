/*
 *  chart.c: high level chart graphics generator
 *
 *  Copyright (C) 1997- John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <gtk/gtk.h>
#include <complex.h>

#include "types.h"

extern 	gboolean printchart;

void		chart_initialize(void);
void		update_cursor_values(int x, int y);
void		draw_swr_circle(double swr);
void		stop_logomode(void);

void		set_point(chart_pt pt, complex val, int ptype);
void		set_point_fixed(chart_point *pt, gboolean fixed);
void		show_point(chart_point *pt);
void		plot_all_points(void);
chart_pt	create_load_point(void);

void		setlinestyle(GnomeCanvasGroup **grp, double lw,
			int color);
void		setfontstyle(GnomeCanvasGroup **grp, char *font,
			double size, int color);

GnomeCanvasItem *do_arc(GnomeCanvasGroup **grp, double x, double y, 
			double arcb, double arce, double rad);
GnomeCanvasItem *do_line(GnomeCanvasGroup **grp, double x1, double y1,
                	double x2, double y2);
GnomeCanvasItem *do_string(GnomeCanvasGroup **grp, char *lbl,
			double x, double y, double size, double angle);

GnomeCanvasItem *rg_arc(GnomeCanvasGroup **grp, complex zb, complex ze,
			double z0, gboolean imp_mode);
GnomeCanvasItem *x_arc(GnomeCanvasGroup **grp, complex zb, complex ze,
			double z0);
GnomeCanvasItem *k_arc(GnomeCanvasGroup **grp, complex zbeg, complex zend,
			double z0);
GnomeCanvasItem *zy_line(GnomeCanvasGroup **grp, complex z);

void		create_printer_chart(void);
void		close_printer_chart(void);

void		update_chart_z0(void);
void		recalculate_all(int dest);

void		test_routine(void);
void		change_zoom(int how);

void		comp_discr_val_scale_value_changed(GtkRange *range);
