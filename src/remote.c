#include <gtk/gtk.h>
#include <fcntl.h>

#include "support.h"
#include "main.h"
#include "global.h"
#include "misc.h"
#include "remote.h"

int
fifos_created(void)
{
  int res;
  struct stat buf;
  
  res = mkfifo(pref.rem_inpipe, 0777);
  if (res != 0) {
    ok_dialog(_("Remote"), _("Cannot create the input FIFO.\n"
              "Possibly the file already exists. "
              "Is another version of linsmith running?\n"
              "I'll continue without remote access."));
    return FALSE;
  }
  res = mkfifo(pref.rem_outpipe, 0777);
  if (res != 0) {
    ok_dialog(_("Remote"), _("Cannot create the output FIFO.\n"
              "Possibly a stale file left over from a previous run.\n"
              "I'll continue without remote access."));
    unlink(pref.rem_inpipe);
    return FALSE;
  }
  return TRUE;
}


int
remove_fifos(gpointer data)
{
  // if not even enabled manually, just go away
  if (pref.rem_mode != REMOTE_SOCK) return;
  
printf("Removing the remote pipes\n");
  close(pref.inpipe);
  close(pref.outpipe);
  
  unlink(pref.rem_inpipe);
  unlink(pref.rem_outpipe);
  return 0;
}


gboolean
remote_inpipe_callback(GIOChannel *ioch, GIOCondition cond, gpointer data)
{
  size_t bytes_read;
  gchar *chr;
  
  g_io_channel_read_line(ioch, &chr, NULL, NULL, NULL);
  printf("%s\n", chr);
  g_free(chr);
  
  return TRUE;
}


void
remote_create_fifos(void)
{
  int inwatch = -1;
  
  // if not even enabled manually, just go away
  if (pref.rem_mode != REMOTE_SOCK) return;
  
  // if enabled, try to get the fifos ready
  if (!fifos_created()) return;
  
  // add the removal function to the main loop
  gtk_quit_add(0, remove_fifos, NULL);

  // Open the input and output pipes, and connect IO channels to them
  pref.inpipe = open(pref.rem_inpipe, O_RDONLY | O_NONBLOCK);
  pref.outpipe = open(pref.rem_outpipe, O_WRONLY | O_NONBLOCK);
  pref.inchannel = g_io_channel_unix_new(pref.inpipe);
  pref.outchannel = g_io_channel_unix_new(pref.outpipe);
  
  // Only for input, connect an event handler
  inwatch = g_io_add_watch(pref.inchannel, G_IO_IN, remote_inpipe_callback, NULL);
  
  return;
}


void
remote_send_command(char *cmd)
{
  char *msg = "Sending!\n";
  gsize written;
  
  printf("Send command called\n");
  g_io_channel_write_chars(pref.outchannel, msg, -1, &written, NULL);
}


int
remote_receive_reply(char *reply)
{
  return FALSE;
}


void
set_remote_enabled(GtkWidget *ref, int state)
{
  GtkWidget *w = lookup_widget(ref, "confrem_frame");
  
  gtk_widget_set_sensitive(w, state);

  w = lookup_widget(MainWindow, "load_getrem_btn");
//  gtk_widget_set_sensitive(w, state);
  g_object_set(G_OBJECT(w), "visible", state, NULL);
}
