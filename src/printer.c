/*
 *  printer.c: Postscript generator - high level routines calling gnome-print
 *
 *  Copyright (C) 1997- John Coppens
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <math.h>
#include <cairo.h>

#include "printer.h"
#include "global.h"
#include "main.h"

#define GET_R(x)        (((x >> 24) & 0xff)/255.0)
#define GET_G(x)        (((x >> 16) & 0xff)/255.0)
#define GET_B(x)        (((x >>  8) & 0xff)/255.0)

#define MM2PTS  (72.0/25.4)

GtkPrintOperation	*op;
GtkPrintSettings	*config = NULL;
cairo_t                 *gpc;
PangoLayout             *layout = NULL;
PangoFontDescription    *prt_last_font_desc = NULL;

double		prt_last_lw = 0.1;
int		prt_last_color = 0,
		prt_last_font_color = 0;


static void
print_begin(GtkPrintOperation *op, GtkPrintContext *context, gpointer data) 
{
  if (debug & DBG_PRINTING)
    fprintf(stderr, "[print_begin]\n");

  gtk_print_operation_set_n_pages(op, 1);
}

static void
print_draw_page(GtkPrintOperation *op, GtkPrintContext *context,
                gint page_nr, gpointer data)
{
  if (debug & DBG_PRINTING)
    fprintf(stderr, "[print_draw_page]\n");
    
  gpc = gtk_print_context_get_cairo_context(context);
  layout = gtk_print_context_create_pango_layout(context);

  create_printer_chart();
  recalculate_all(DST_PRINT);
}

static void
print_end(GtkPrintOperation *op, GtkPrintContext *context, gpointer data) 
{
  if (debug & DBG_PRINTING)
    fprintf(stderr, "[print_end]\n");
}


void
print_chart(char *fn)
{
  GError *err = NULL;
  GtkPaperSize *papersize = NULL;
  gint res;
  
  if (debug & DBG_PRINTING)
    fprintf(stderr, "[print_chart]\n");

  op = gtk_print_operation_new();
  gtk_print_operation_set_unit(op, GTK_UNIT_MM);
  if (config != NULL) {
    papersize = gtk_paper_size_new(pref.prt_papersize);
    gtk_print_settings_set_paper_size(config, papersize);
    gtk_print_operation_set_print_settings(op, config);
    gtk_paper_size_free(papersize);
  }
  
  g_signal_connect(G_OBJECT(op), "begin-print", 
                G_CALLBACK(print_begin), NULL); 
  g_signal_connect(G_OBJECT(op), "draw-page", 
                G_CALLBACK(print_draw_page), NULL); 
  g_signal_connect(G_OBJECT(op), "end-print", 
                G_CALLBACK(print_end), NULL); 

  gtk_print_operation_set_use_full_page(op, FALSE); 
  gtk_print_operation_set_unit(op, GTK_UNIT_MM); 
  
  res = gtk_print_operation_run(op, 
                GTK_PRINT_OPERATION_ACTION_PRINT_DIALOG, 
                GTK_WINDOW(MainWindow), 
                &err); 
  
  if (res == GTK_PRINT_OPERATION_RESULT_APPLY) {
    if (config != NULL)
      g_object_unref(config);
    config = g_object_ref(gtk_print_operation_get_print_settings(op));
  } else {
    if (err) {
      GtkWidget *dialog;
 	
      dialog = gtk_message_dialog_new(GTK_WINDOW(MainWindow),
                GTK_DIALOG_DESTROY_WITH_PARENT,
                GTK_MESSAGE_ERROR,
                GTK_BUTTONS_CLOSE,
                "%s", err->message);
      g_error_free(err);
 	
      g_signal_connect(dialog, "response",
                G_CALLBACK(gtk_widget_destroy), NULL);
 	
      gtk_widget_show(dialog);
    }
  } 

  g_object_unref(op); 
}


void
print_setrgbcolor(int color)
{
  double r = GET_R(color),
         g = GET_G(color),
         b = GET_B(color);

  if (debug & DBG_PRINTING)
    fprintf(stderr, "[prt color] r:%f g:%f b:%f\n", r, g, b);

  cairo_set_source_rgb(gpc, r, g, b);
}


void
print_do_arc(double x, double y, double arcb, double arce, double rad)
{
  double ctrx, ctry, begx, begy, arcrad,
	 chrad = pref.prt_chartsize * 0.5;

  if (debug & DBG_PRINTING)
    fprintf(stderr, "[src] x:%f y:%f beg:%f end:%f rad:%f\n",
    		    x, y, arcb, arce, rad);

  arcrad = chrad * rad;
  ctrx = x * chrad;
  ctry = (1 - y) * chrad;
  begx = ctrx + arcrad * cos(arcb);
  begy = ctry - arcrad * sin(arcb);

  cairo_new_path(gpc);
  cairo_set_line_width(gpc, prt_last_lw);
  cairo_set_source_rgb(gpc,
                GET_R(prt_last_color), 
                GET_G(prt_last_color),
                GET_B(prt_last_color));
  cairo_move_to(gpc, begx, begy);
  cairo_arc_negative(gpc, ctrx, ctry, arcrad, -arcb, -arce);
  cairo_stroke(gpc);
}


void
print_do_line(double x1, double y1, double x2, double y2)
{
  double chrad = pref.prt_chartsize * 0.5;

  if (debug & DBG_PRINTING)
    fprintf(stderr, "[line] x1:%f y1:%f x2:%f y2:%f\n",
    		    x1, y1, x2, y2);
  
  cairo_new_path(gpc);
  cairo_set_source_rgb(gpc,
                GET_R(prt_last_color),
                GET_G(prt_last_color),
                GET_B(prt_last_color));
  cairo_set_line_width(gpc, prt_last_lw);
  cairo_move_to(gpc, chrad * x1, chrad * (1 - y1));
  cairo_line_to(gpc, chrad * x2, chrad * (1 - y2));
  cairo_stroke(gpc);
}


void
print_do_string(char *str, double x, double y, 
		double size, double angle)
{
  double chrad = pref.prt_chartsize * 0.5,
         text_height;
  int layout_height;

  if (debug & DBG_PRINTING)
    fprintf(stderr, "[string] str:[%s] x:%f y:%f size:%f angle:%f\n",
                    str, x, y, size, angle);

  pango_layout_set_font_description(layout, prt_last_font_desc);
  pango_layout_set_text(layout, str, -1);
  pango_layout_set_width(layout, -1);
  pango_layout_set_alignment(layout, PANGO_ALIGN_LEFT);
  pango_layout_get_size (layout, NULL, &layout_height);
  text_height = (gdouble) layout_height / PANGO_SCALE;
  print_setrgbcolor(prt_last_font_color);

  cairo_save(gpc);
    cairo_translate(gpc, chrad * x, chrad * (1 - y));
    cairo_rotate(gpc, -angle);
    cairo_move_to(gpc, text_height/10.0, -text_height);
    pango_cairo_show_layout(gpc, layout);
  cairo_restore(gpc);
}


void
print_point(double x, double y, int pstyle)
{
  double r = pref.prt_diam[pstyle] / 2,
	 chrad = pref.prt_chartsize * 0.5;

  if (debug & DBG_PRINTING) {
    fprintf(stderr, "[prt pt] x:%f y:%f r:%f\n", x, y, r);
    fprintf(stderr, "         bc:%08x fc:%08x\n", 
			pref.prt_fill_color[pstyle],
			pref.prt_brdr_color[pstyle]);
  }
  cairo_new_path(gpc);
  cairo_move_to(gpc, x * chrad + r, (1 - y) * chrad);
  cairo_arc    (gpc, x * chrad,     (1 - y) * chrad, r, 0, 2*M_PI);
  cairo_close_path(gpc);

  cairo_set_source_rgb(gpc, 
                GET_R(pref.prt_fill_color[pstyle]),
                GET_G(pref.prt_fill_color[pstyle]),
                GET_B(pref.prt_fill_color[pstyle]));
  cairo_fill(gpc);

  cairo_new_path(gpc);
  cairo_move_to(gpc, x * chrad + r, (1 - y) * chrad);
  cairo_arc    (gpc, x * chrad,     (1 - y) * chrad, r, 0, 2*M_PI);
  cairo_close_path(gpc);
  
  cairo_set_source_rgb(gpc,
                GET_R(pref.prt_brdr_color[pstyle]),
                GET_G(pref.prt_brdr_color[pstyle]),
                GET_B(pref.prt_brdr_color[pstyle]));
  cairo_set_line_width(gpc, pref.prt_brdr[pstyle]);
  cairo_stroke(gpc);
}


void
print_setlinestyle(double lw, int color)
{
  prt_last_lw = lw;
  prt_last_color = color;
}


gboolean
print_setfontstyle(char *font, double size, int color)
{
  prt_last_font_color = color;
  
  prt_last_font_desc = pango_font_description_from_string(font);
  pango_font_description_set_absolute_size(prt_last_font_desc, size * PANGO_SCALE);

  return TRUE;
}
