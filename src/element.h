/*
 *  element.c: Element related routines
 *
 *  Copyright (C) 1997-2005 John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <gtk/gtk.h>
#include <math.h>

#include "types.h"

enum {ELT_CAP,   ELT_IND,   ELT_PARLC, ELT_SERLC,
      ELT_OSTUB, ELT_SSTUB, ELT_LINE,
      ELT_XFORM, ELT_ZY,
      ELT_EMPTY};
      
enum {ELC_SERIES, ELC_PARALLEL,
      ELC_EMPTY};
      
enum {CIRCLIST_SELECT, CIRCLIST_UNSELECT, CIRCLIST_EMPTY,
      CIRCLIST_ENTRYVALID, CIRCLIST_ENTRYINVALID,
      CIRCLIST_ENTRYSET, CIRCLIST_APPEND, CIRCLIST_UPDATED,
      CIRCLIST_CONNCHANGED, CIRCLIST_TYPECHANGED};
      
enum {EL_CONN, EL_TYPE, 			// conn, type
      EL_VAL1,					// value
      EL_DESCR,					// descr
      EL_USE_LOSS,				// useloss
      EL_CONN_VAL,  EL_TYPE_VAL,		// conn, type
      EL_VAL_VAL1,  EL_VAL_VAL2,		// val1, val2
      EL_Z0_VAL, EL_VF_VAL,			// z0, vf
      EL_LOSS1_VAL, EL_MHZ1_VAL,		// loss1, mhz1
      EL_LOSS2_VAL, EL_MHZ2_VAL,		// loss2, mhz2
      EL_POINTLIST, EL_LINELIST, EL_ACT_ITER,	// ptlist, linlist, act
      EL_COLS};

extern	GtkWidget *el_view;
extern	el_definition act_el;
extern	GtkTreeIter active_el_iter;
      
void  	load3_activate(void);
void	save_as4_activate(void);
void	save4_activate(void);

void	initialize_elementtable(void);
int	fetch_selected_element(GtkTreeView *treeview, el_definition *el);

int     element_valid(el_definition *el);
void    element_update(el_definition el);
void	element_append(el_definition el);
int	number_elements(void);

void    active_element_update(el_definition el);
void	active_element_remove(void);
void	circlist_clear();
void	circ_move_up(void);
void	circ_move_down(void);

void	load_circuit(char *fn);
void	save_circuit(char *fn);

complex	calc_line_impedance(complex zl, double z0, double len, double f);
complex	calc_el_impedance(el_definition *el, double f);
void	set_discr_mode(int mode);
void	clear_element_ptlist(chart_ptlist *lst);
void    circ_treeview_cursor_changed(GtkTreeView *treeview);
void    comp_stub_selcable_btn_clicked(void);
void    comp_line_selcable_btn_clicked(void);
