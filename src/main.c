/*
 *  main.c  -- The Main Program.
 *
 *  Copyright (C) 1997- John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <getopt.h>

#include "interface.h"
#include "support.h"
#include "global.h"

GtkWidget 	*MainWindow;
GtkWidget	*ImportWindow;
int		debug = 0;

void
check_my_params(char *dbg_args)
{
  int c;

  if (dbg_args == NULL) return;

  for (c = 0; c < strlen(dbg_args); c++) {
    switch (dbg_args[c]) {
      case 'l': debug |= DBG_LOADS; break;
      case 'e': debug |= DBG_ELEMENTS; break;
      case 'g': debug |= DBG_GRAPHICS; break;
      case 'p': debug |= DBG_PRINTING; break;
      case 'm': debug |= DBG_MATH; break;
      case 'o': debug |= DBG_LOGGING; break;
    }
  }
}

int
main (int argc, char *argv[])
{
  gboolean fatal_warnings = FALSE;
#ifdef ENABLE_NLS
  bindtextdomain (GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
#endif

  gchar *dbg = NULL;
  const GOptionEntry opt_entries[] = {
    {"debug", 'd', 0, G_OPTION_ARG_STRING, &dbg, 
             N_("Debug options:\n"
                "                                    e: element\n"
                "                                    l: loads\n"
                "                                    p: printing\n"
                "                                    g: graphics\n"
                "                                    m: math\n"
                "                                    l: logging"), 
             N_("opts")},
    {NULL}
  };
  GOptionContext *option_context;
  GError *err = NULL;

  option_context = g_option_context_new(_("\n  (c) John Coppens 1997-2011"));
  g_option_context_add_main_entries(option_context, opt_entries, GETTEXT_PACKAGE);
  g_option_context_set_translation_domain(option_context, GETTEXT_PACKAGE);
  g_option_context_set_ignore_unknown_options(option_context, TRUE);
  g_option_context_parse(option_context, &argc, &argv, &err);

  if (err) {
    g_print("option parsing failed: %s\n", err->message);
    exit(1);
  }

  gnome_program_init(PACKAGE, VERSION, LIBGNOMEUI_MODULE,
                argc, argv,
                GNOME_PARAM_APP_DATADIR, PACKAGE_DATA_DIR,
                GNOME_PARAM_GOPTION_CONTEXT, option_context,
                NULL);
                
  g_option_context_free(option_context);
                
  check_my_params(dbg);
                      
  /*
   * The following code was added by Glade to create one of each component
   * (except popup menus), just so that you see something after building
   * the project. Delete any components that you don't want shown initially.
   */
  MainWindow = create_MainWindow();
  gtk_widget_show(MainWindow);

  gtk_main ();
  return 0;
}

