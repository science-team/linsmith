/*
 *  confmgr.c: configuration manager
 *
 *  Copyright (C) 1997- John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "confmgr.h"
#include "global.h"
#include "support.h"

char *
combo_box_get_active_text(GtkComboBox *cbbox)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  int active;
  char *size;
  
  active = gtk_combo_box_get_active(cbbox);
  if (active < 0)
    return NULL;
    
  model = gtk_combo_box_get_model(cbbox);
  gtk_tree_model_iter_nth_child(model, &iter, NULL, active);
  gtk_tree_model_get(model, &iter,
                1, &size,                       // The 'internal' formal name
                -1);
  return size;
}

int
combo_box_set_active_text(GtkComboBox *cbbox, char *str)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  gboolean ok;
  int found, elnr;
  char *size;

  if (!str || (strlen(str) == 0))
    return -1;

  model = gtk_combo_box_get_model(cbbox);
  found = 0; elnr = 0;
  ok = gtk_tree_model_get_iter_first(model, &iter);
  while (ok) {
    gtk_tree_model_get(model, &iter,
                1, &size,                       // This is the 'internal' name
                -1);
    if (strcmp(size, str) == 0) {
      found = elnr;
      g_free(size);
      break;
    }
    g_free(size);
    elnr++;
    ok = gtk_tree_model_iter_next(model, &iter);
  }
  gtk_combo_box_set_active(cbbox, found);
}

void
parse_widget_by_table(conf_definition *tbl, GtkWidget *ref)
{
  GtkWidget *w;
  GList *glst;
  GdkColor col;
  char *p;
  int d, nr;
  guint16 alpha;

  for (d = 0; tbl[d].typ != CFM_W_LAST; d++) {
    printf("%d\n", d);
    w = lookup_widget(ref, tbl[d].wdg);
    switch (tbl[d].typ) {
      case CFM_W_CBTN: 
          *(char *)tbl[d].data =
                 gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w));
          break;
      case CFM_W_ENTRY_STR:
	  p = (char *)gtk_entry_get_text(GTK_ENTRY(w));
          tbl[d].data = 
		g_locale_from_utf8(p, -1, NULL, NULL, NULL);
          break;
      case CFM_W_ENTRY_INT:
          *(int *)tbl[d].data = atol(gtk_entry_get_text(GTK_ENTRY(w)));
          break;
      case CFM_W_ENTRY_FLOAT:
          *(float *)tbl[d].data = atof(gtk_entry_get_text(GTK_ENTRY(w)));
          break;
      case CFM_W_ENTRY_DOUBLE:
          *(double *)tbl[d].data = atof(gtk_entry_get_text(GTK_ENTRY(w)));
          break;
      case CFM_W_LABEL_STR:
          strcpy((char *)tbl[d].data, gtk_label_get_text(GTK_LABEL(w)));
          break;
      case CFM_W_LABEL_INT:
          *(int *)tbl[d].data = atol(gtk_label_get_text(GTK_LABEL(w)));
          break;
      case CFM_W_LABEL_DOUBLE:
          *(double *)tbl[d].data = atof(gtk_label_get_text(GTK_LABEL(w)));
          break;
      case CFM_W_RBTN: 
          if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(w)))
                       *(int *)tbl[d].data = tbl[d].misc;
          break;
      case CFM_W_SPBTN_INT: 
          *(int *)tbl[d].data =
                  gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(w));
          break;
      case CFM_W_SPBTN_FLOAT: 
          *(float *)tbl[d].data =
                  gtk_spin_button_get_value_as_float(GTK_SPIN_BUTTON(w));
          break;
      case CFM_W_SPBTN_DOUBLE:
          *(double *)tbl[d].data =
                  gtk_spin_button_get_value(GTK_SPIN_BUTTON(w));
          break;
      case CFM_W_CBOX:
	  *(int *)tbl[d].data =
		  gtk_combo_box_get_active(GTK_COMBO_BOX(w));
	  break;
      case CFM_W_COMBOBOX_STR:
          tbl[d].data =
                  combo_box_get_active_text(GTK_COMBO_BOX(w));
          break;
      case CFM_W_CLIST:
          glst = *(GList **)tbl[d].data;
          if (glst != NULL) {
            g_list_foreach(glst, (GFunc)g_free, NULL);
            g_list_free(glst);
            glst = NULL;
          }
          for (nr = 0; nr < GTK_CLIST(w)->rows; nr++) {
            gtk_clist_get_text(GTK_CLIST(w), nr, 0, &p);
            glst = g_list_append(glst, strdup(p));
          }
          *(GList **)tbl[d].data = glst;
          break;
      case CFM_W_COLORBUTTON:
	  gtk_color_button_get_color(GTK_COLOR_BUTTON(w), &col);
	  alpha = gtk_color_button_get_alpha(GTK_COLOR_BUTTON(w));
          *(int *)tbl[d].data =	(col.red >> 8)  *0x1000000 + 
				(col.green >> 8)*0x10000 + 
				(col.blue >> 8) *0x100 + 
				(alpha >> 8);
          break;
      case CFM_W_FONTBUTTON:
          strcpy((char *)tbl[d].data,
		 gtk_font_button_get_font_name(GTK_FONT_BUTTON(w)));
          break;
    }
  }
}


void
load_widget_by_table(conf_definition *tbl, GtkWidget *ref)
{
  GtkWidget *w;
  GList *glst;
  GdkColor col;
  int d, nr;
  guint16 alpha;
  double val;
  char bff[15], *str[1], *p;

  for (d = 0; tbl[d].typ != CFM_W_LAST; d++) {
    w = lookup_widget(ref, tbl[d].wdg);
    switch (tbl[d].typ) {
      case CFM_W_CBTN: 
          gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(w), 
                                                 *(char *)tbl[d].data);
          break;
      case CFM_W_ENTRY_STR:
          p = (char *)tbl[d].data;
          gtk_entry_set_text(GTK_ENTRY(w), 
		p = g_locale_to_utf8(p, -1, NULL, NULL, NULL));
	  g_free(p);
          break;
      case CFM_W_ENTRY_INT: 
          sprintf(bff, "%d", *(int *)tbl[d].data);
          gtk_entry_set_text(GTK_ENTRY(w), bff);
          break;
      case CFM_W_ENTRY_FLOAT:
          sprintf(bff, "%f", *(float *)tbl[d].data);
          gtk_entry_set_text(GTK_ENTRY(w), bff);
          break;
      case CFM_W_ENTRY_DOUBLE:
          sprintf(bff, "%.*f", tbl[d].misc, *(double *)tbl[d].data);
          gtk_entry_set_text(GTK_ENTRY(w), bff);
          break;
      case CFM_W_LABEL_STR:
          gtk_label_set_text(GTK_LABEL(w), (char *)tbl[d].data);
          break;
      case CFM_W_LABEL_INT: 
          sprintf(bff, "%d", *(int *)tbl[d].data);
          gtk_label_set_text(GTK_LABEL(w), bff);
          break;
      case CFM_W_LABEL_DOUBLE:
          sprintf(bff, "%.*f", tbl[d].misc, *(double *)tbl[d].data);
          gtk_label_set_text(GTK_LABEL(w), bff);
          break;
      case CFM_W_RBTN: 
          if (*(char *)tbl[d].data & tbl[d].misc)
                  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(w), TRUE);
          break;
      case CFM_W_SPBTN_INT:
          val = *(int *)tbl[d].data;
          gtk_spin_button_set_value(GTK_SPIN_BUTTON(w), val);
          break;
      case CFM_W_SPBTN_FLOAT:
          val = *(float *)tbl[d].data;
          gtk_spin_button_set_value(GTK_SPIN_BUTTON(w), val);
          break;
      case CFM_W_SPBTN_DOUBLE:
          val = *(double *)tbl[d].data;
          gtk_spin_button_set_value(GTK_SPIN_BUTTON(w), val);
          break;
      case CFM_W_CBOX:
	  gtk_combo_box_set_active(GTK_COMBO_BOX(w), *(char *)tbl[d].data);
	  break;
      case CFM_W_COMBOBOX_STR:
	  combo_box_set_active_text(GTK_COMBO_BOX(w), (char *)tbl[d].data);
	  break;
      case CFM_W_CLIST:
          glst = *(GList **)tbl[d].data;
          for (nr = 0; nr < g_list_length(glst); nr++) {
            str[0] = g_list_nth_data(glst, nr);
            gtk_clist_append(GTK_CLIST(w), str);
          }
          break;
      case CFM_W_COLORBUTTON:
          alpha     = ( *(guint32 *)tbl[d].data        & 0xff) << 8;
          col.blue  = ((*(guint32 *)tbl[d].data >>  8) & 0xff) << 8;
          col.green = ((*(guint32 *)tbl[d].data >> 16) & 0xff) << 8;
          col.red   = ((*(guint32 *)tbl[d].data >> 24) & 0xff) << 8;
          gtk_color_button_set_color(GTK_COLOR_BUTTON(w), &col);
	  gtk_color_button_set_alpha(GTK_COLOR_BUTTON(w), alpha);
          break;
      case CFM_W_FONTBUTTON:
          gtk_font_button_set_font_name(GTK_FONT_BUTTON(w),
		(char *)tbl[d].data);
          break;
    }
  }
}


gboolean
save_by_table(char *fn, conf_definition *tbl)
{
  char path[120], path2[140];
  int d, nr;
  GList *glst;

  for (d = 0; tbl[d].typ != CFM_W_LAST; d++) {
    if (fn != NULL)
      sprintf(path, "=%s=/%s", fn, tbl[d].key);
    else
      sprintf(path, "%s/%s", prog_name, tbl[d].key);

    switch (tbl[d].styp) {
      case CFM_T_STR: 
          gnome_config_set_string(path, (char *)tbl[d].data);
          break;
      case CFM_T_CHAR:
          gnome_config_set_int(path, *(char *)tbl[d].data);
          break;
      case CFM_T_INT: 
          gnome_config_set_int(path, *(int *)tbl[d].data);
          break;
      case CFM_T_FLOAT: 
          gnome_config_set_float(path, *(float *)tbl[d].data);
	  break;
      case CFM_T_DOUBLE:
          gnome_config_set_float(path, *(double *)tbl[d].data);
          break;
      case CFM_T_STRLIST:
          sprintf(path2, "%s_nr", path);
          glst = *(GList **)tbl[d].data;
          gnome_config_set_int(path2, g_list_length(glst));
          for (nr = 0; nr < g_list_length(glst); nr++) {
            sprintf(path2, "%s_%d", path, nr);
            gnome_config_set_string(path2, g_list_nth_data(glst, nr));
          }
          break;
    }
  }

  return gnome_config_sync();
}

void
load_by_table(char *fn, conf_definition *tbl)
{
  GList *glst;
  char path[120], path2[140], *p;
  int def;
  int d, nr, llen, v;

  for (d = 0; tbl[d].typ != CFM_W_LAST; d++) {
    if (fn != NULL)
      sprintf(path, "=%s=/%s", fn, tbl[d].key);
    else
      sprintf(path, "%s/%s", prog_name, tbl[d].key);
      
    switch (tbl[d].styp) {
      case CFM_T_STR: 
          sprintf(path2, "%s=%s", path, tbl[d].def);
           
	  p = gnome_config_get_string_with_default(path2, &def);
          strcpy(tbl[d].data, p);
          g_free(p);
          break;
      case CFM_T_INT: 
          v = strtol(tbl[d].def, NULL, 0);
          sprintf(path2, "%s=%d", path, v);
          *(int *)tbl[d].data =
		gnome_config_get_int_with_default(path2, &def);
          break;
      case CFM_T_CHAR:
          sprintf(path2, "%s=%s", path, tbl[d].def);
          *(char *)tbl[d].data =
		gnome_config_get_int_with_default(path2, &def);
          break;
      case CFM_T_FLOAT: 
          sprintf(path2, "%s=%s", path, tbl[d].def);
          *(float *)tbl[d].data =
                          gnome_config_get_float_with_default(path2, &def);
          break;
      case CFM_T_DOUBLE:
          sprintf(path2, "%s=%s", path, tbl[d].def);
          *(double *)tbl[d].data =
                          gnome_config_get_float_with_default(path2, &def);
          break;
      case CFM_T_STRLIST:
          glst = *(GList **)tbl[d].data;
          sprintf(path2, "%s_nr=0", path);
          llen = gnome_config_get_int_with_default(path2, &def);

          for (nr = 0; nr < llen; nr++) {
            sprintf(path2, "%s_%d=", path, nr);
            p = gnome_config_get_string_with_default(path2, &def);
            glst = g_list_append(glst, p);
          }
          *(GList **)tbl[d].data = glst;
          break;
    }
  }
}
