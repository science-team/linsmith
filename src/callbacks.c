/*
 *  callbacks.c: event and signal handlers
 *
 *  Copyright (C) 1997-2005 John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>
#include <complex.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

#include "main.h"
#include "types.h"
#include "global.h"
#include "printer.h"
#include "chart.h"
#include "element.h"
#include "load.h"
#include "misc.h"


//=====================================================================
//	Main menu
//=====================================================================

//	File menu item
//	Circuit:

void
on_load3_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  load3_activate();
}


void
on_save4_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  save4_activate();
}


void
on_save_as4_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  save_as4_activate();
}

//	Loads:

void
on_load4_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  load4_activate();
}


void
on_save5_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  save5_activate();
}


void
on_save_as3_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  save_as3_activate();
}


void
on_csv_loads1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  import_csv_activate();
}


void
on_s2p_files1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  s2p_files1_activate();
}


void
on_save_results_page1_activate         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  loglist_export();
}


void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  gtk_main_quit();
}

//	Edit menu items

void
on_preferences1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  GtkWidget *w = create_ConfigWindow();
  gtk_widget_show(w);
}

//	View menu items

void
on_recalculate1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}

//	Help menu item

void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  show_about();
}

//---------------------------------------------------------------------
//	Other MainWindow events
//---------------------------------------------------------------------

gboolean
on_MainWindow_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{
  gtk_main_quit();
  return FALSE;
}


void
on_MainWindow_realize                  (GtkWidget       *widget,
                                        gpointer         user_data)
{
  initialize_MainWindow();
  initialize_elementtable();
  initialize_loadtable();
}


void
on_zoom_out_btn_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{
  change_zoom(ZOOM_OUT);
}


void
on_zoom_in_btn_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{
  change_zoom(ZOOM_IN);
}


void
on_recalc_btn_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{
  recalculate_all(DST_SCREEN);
}


void
on_z0_cbbox_changed                    (GtkComboBox     *combobox,
                                        gpointer         user_data)
{
  recalculate_all(DST_SCREEN);
  save_config();
}


void
on_z0_cbbox_realize                    (GtkWidget       *widget,
                                        gpointer         user_data)
{
  gtk_combo_box_set_active(GTK_COMBO_BOX(widget), 0);
}


void
on_direction_cbbox_changed             (GtkComboBox     *combobox,
                                        gpointer         user_data)
{
  int prev_rot = pref.rotation;

  switch (gtk_combo_box_get_active(combobox)) {
    case 0: pref.rotation = TO_GENERATOR;
	    break;
    case 1: pref.rotation = TO_LOAD;
	    break;
  }
  if (prev_rot != pref.rotation)
    recalculate_all(DST_SCREEN);
}


void
on_direction_cbbox_realize             (GtkWidget       *widget,
                                        gpointer         user_data)
{
  gtk_combo_box_set_active(GTK_COMBO_BOX(widget), 0);
}


//---------------------------------------------------------------------
//      Main window, CHART events
//---------------------------------------------------------------------

void
on_canvas1_realize                     (GtkWidget       *widget,
                                        gpointer         user_data)
{
  chart_initialize();
}


gboolean
on_chart_canvas_motion_notify_event    (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data)
{
  if (event->device->source == GDK_SOURCE_MOUSE) {
    update_cursor_values(event->x, event->y);
  }
  return FALSE;
}

//---------------------------------------------------------------------
//      Main page notebook
//---------------------------------------------------------------------

void
on_circuit_notebook_realize            (GtkWidget       *widget,
                                        gpointer         user_data)
{

}

//	LOAD page

void
on_load_treeview_cursor_changed        (GtkTreeView     *treeview,
                                        gpointer         user_data)
{
  on_load_selected(treeview);
}


void
on_load_append_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{
  load_definition tload;

  if (load_rx_valid(&tload)) load_append(&tload);
}


void
on_load_remove_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{
  active_load_rx_remove();
}


void
on_load_clear_btn_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{
  loadlist_clear();
}


void
on_load_edit_btn_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
  if (load_rx_valid(NULL)) active_load_rx_update();
}


void
on_load_rx_rbtn_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_load_rang_rbtn_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_load_x_entry_changed                (GtkEditable     *editable,
                                        gpointer         user_data)
{
  load_modified(2);
}


void
on_load_r_entry_changed                (GtkEditable     *editable,
                                        gpointer         user_data)
{
  load_modified(1);
}


void
on_load_freq_entry_changed             (GtkEditable     *editable,
                                        gpointer         user_data)
{
  load_modified(0);
}

//
//	Noise bridge events
//

void
on_nb_type_cbbox_changed               (GtkComboBox     *combobox,
                                        gpointer         user_data)
{

}


void
on_nb_type_cbbox_realize               (GtkWidget       *widget,
                                        gpointer         user_data)
{

}


void
on_load_nb_r_entry_changed             (GtkEditable     *editable,
                                        gpointer         user_data)
{
  load_nb_modified(1);
}


void
on_load_nb_c_entry_changed             (GtkEditable     *editable,
                                        gpointer         user_data)
{
  load_nb_modified(2);
}


void
on_load_nb_ext_cbtn_toggled            (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{
  load_nb_modified(3);
}


void
on_load_nb_append_btn_clicked          (GtkButton       *button,
                                        gpointer         user_data)
{
  load_definition tload;
 
  if (load_nb_valid(&tload)) load_append(&tload);
}


void
on_load_nb_upd_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{
  if (load_nb_valid(NULL)) active_load_nb_update();
}


void
on_load_nb_freq_entry_changed          (GtkEditable     *editable,
                                        gpointer         user_data)
{
  load_nb_modified(0);
}



void
on_comp_stub_selcable_btn_clicked      (GtkButton       *button,
                                        gpointer         user_data)
{
  comp_stub_selcable_btn_clicked();
}


void
on_comp_line_selcable_btn_clicked      (GtkButton       *button,
                                        gpointer         user_data)
{
  comp_line_selcable_btn_clicked();
}


//	CIRCUIT page

void
on_circ_newel_btn_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{
  el_definition el;
  if (element_valid(&el)) element_append(el);
}


void
on_circ_upd_el_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{
  el_definition el;
  if (element_valid(&el)) element_update(el);
}


void
on_circ_delete_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{
  active_element_remove();
}


void
on_circ_clear_btn_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{
  circlist_clear();
}


void
on_circ_up_btn_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{
  circ_move_up();
}


void
on_circ_down_btn_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{
  circ_move_down();
}


void
on_circ_treeview_cursor_changed        (GtkTreeView     *treeview,
                                        gpointer         user_data)
{
  circ_treeview_cursor_changed(treeview);
}


void
on_comp_series_tbtn_toggled            (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{

}


void
on_comp_parallel_tbtn_toggled          (GtkToggleButton *togglebutton,
                                        gpointer         user_data)
{

}

//=====================================================================
//      Config menu
//=====================================================================

void
on_conf_cancel_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{
  gtk_widget_destroy(lookup_widget(GTK_WIDGET(button), "ConfigWindow"));
}


void
on_conf_ok_btn_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{
  parse_configwindow(GTK_WIDGET(button));
  save_config();
  stop_logomode();
  draw_swr_circle(pref.swr_circle);	// Must always redraw
  gtk_widget_destroy(lookup_widget(GTK_WIDGET(button), "ConfigWindow"));
}


void
on_ConfigWindow_realize                (GtkWidget       *widget,
                                        gpointer         user_data)
{
  load_configwindow(widget);
}


gboolean
on_ConfigWindow_destroy_event          (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_log_treeview_realize                (GtkWidget       *widget,
                                        gpointer         user_data)
{
  loglist_initialize(GTK_TREE_VIEW(widget));
}

void
on_notebook4_realize                   (GtkWidget       *widget,
                                        gpointer         user_data)
{
  gtk_widget_realize(lookup_widget(MainWindow, "log_treeview"));
}


void
on_save_as_postscript1_activate        (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_prt_font_picker_font_set            (GnomeFontPicker *fontpicker,
                                        gchar           *font_name,
                                        gpointer         user_data)
{
  GtkWidget *w = lookup_widget(GTK_WIDGET(fontpicker), "prt_font_entry");

  gtk_entry_set_text(GTK_ENTRY(w), font_name);
}


void
on_comp_xform_entry_changed            (GtkEditable     *editable,
                                        gpointer         user_data)
{
  GtkWidget *w = lookup_widget(MainWindow, "circ_edit_btn");

  g_signal_emit_by_name(G_OBJECT(w), "clicked", NULL);
}


void
on_load_getrem_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data)
{
}

//================== Test only ===================================

void
on_test1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  test_routine();
}

//=====================================================================
//      Predefined cable selection window
//=====================================================================

void
on_loadtype_notebook_switch_page       (GtkNotebook     *notebook,
                                        GtkNotebookPage *page,
                                        guint            page_num,
                                        gpointer         user_data)
{
  switch_load_mode(page_num);
}


void
on_rem_cancel_btn_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{

}

//
//	Import
//


gboolean
on_Imports2pWindow_delete_event        (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_Imports2pWindow_realize             (GtkWidget       *widget,
                                        gpointer         user_data)
{
  Imports2pWindow_realize(widget);
}


void
on_import_s2p_sel_all_btn_clicked          (GtkButton       *button,
                                        gpointer         user_data)
{
  import_s2p_sel_all_btn_clicked();
}


void
on_import_s2p_sel_none_btn_clicked         (GtkButton       *button,
                                        gpointer         user_data)
{
  import_s2p_sel_none_btn_clicked();
}


void
on_import_s2p_sel_invert_btn_clicked       (GtkButton       *button,
                                        gpointer         user_data)
{
  import_s2p_sel_invert_btn_clicked();
}


void
on_import_s2p_cancel_btn_clicked           (GtkButton       *button,
                                        gpointer         user_data)
{
  import_s2p_cancel_btn_clicked();
}


void
on_import_s2p_impS11_btn_clicked       (GtkButton       *button,
                                        gpointer         user_data)
{
  import_s2p_impS11_btn_clicked();
}


void
on_import_s2p_impS22_btn_clicked       (GtkButton       *button,
                                        gpointer         user_data)
{
  import_s2p_impS22_btn_clicked();
}


void
on_print1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{
  print_chart(pref.last_ps_file);
}

void
on_cable_remove_btn_clicked            (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_cable_add_btn_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_cable_edit_btn_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{

}

