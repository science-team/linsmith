/*
 *  callbacks.c: event and signal handlers
 *
 *  Copyright (C) 1997-2005 John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <gnome.h>


void
on_load3_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save4_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save_as4_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_load4_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save5_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save_as3_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_preferences1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_about1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

gboolean
on_MainWindow_delete_event             (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_load_append_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data);

void
on_load_remove_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data);

void
on_load_edit_btn_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_circ_up_btn_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_circ_down_btn_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_load_rx_rbtn_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_load_rang_rbtn_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_canvas1_realize                     (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_circ_par_cline_tbtn_clicked         (GtkButton       *button,
                                        gpointer         user_data);

void
on_circ_treeview_cursor_changed        (GtkTreeView     *treeview,
                                        gpointer         user_data);

void
on_load_treeview_cursor_changed        (GtkTreeView     *treeview,
                                        gpointer         user_data);

void
on_circ_rota_rbtn_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_circ_rotr_rbtn_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_circ_spec_rbtn_clicked              (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_chart_canvas_motion_notify_event    (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data);

void
on_MainWindow_realize                  (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_circuit_notebook_realize            (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_recalc_btn_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_load_setref_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data);

void
on_recalculate1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_conf_cancel_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data);

void
on_conf_ok_btn_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_ConfigWindow_realize                (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_test1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_zoom_out_btn_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_zoom_in_btn_clicked                 (GtkButton       *button,
                                        gpointer         user_data);

void
on_log_treeview_realize                (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_notebook4_realize                   (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_save_as_postscript1_activate        (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_prt_font_picker_font_set            (GnomeFontPicker *fontpicker,
                                        gchar           *font_name,
                                        gpointer         user_data);

gboolean
on_ConfigWindow_destroy_event          (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_load_getrem_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data);

void
on_confrem_enable_cbtn_clicked         (GtkButton       *button,
                                        gpointer         user_data);

void
on_z0_entry_changed                    (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_circ_newel_btn_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_circ_delete_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data);

void
on_circ_upd_el_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data);

void
on_comp_stub_selcable_btn_clicked      (GtkButton       *button,
                                        gpointer         user_data);

void
on_CableWindow_realize                 (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_comp_line_selcable_btn_clicked      (GtkButton       *button,
                                        gpointer         user_data);

void
on_cable_view_cursor_changed           (GtkTreeView     *treeview,
                                        gpointer         user_data);

void
on_cable_cancel_btn_clicked            (GtkButton       *button,
                                        gpointer         user_data);

void
on_cable_accept_btn_clicked            (GtkButton       *button,
                                        gpointer         user_data);

void
on_save_results_page1_activate         (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_comp_series_tbtn_toggled            (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_comp_parallel_tbtn_toggled          (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_z0_entry_editing_done               (GtkCellEditable *celleditable,
                                        gpointer         user_data);

void
on_z0_cbbox_changed                    (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_z0_cbbox_realize                    (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_load_x_entry_changed                (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_load_r_entry_changed                (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_load_freq_entry_changed             (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_load_clear_btn_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_circ_clear_btn_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_direction_cbbox_changed             (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_direction_cbbox_realize             (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_loadtype_notebook_switch_page       (GtkNotebook     *notebook,
                                        GtkNotebookPage *page,
                                        guint            page_num,
                                        gpointer         user_data);

void
on_rem_cancel_btn_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_nb_type_cbbox_changed               (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_nb_type_cbbox_realize               (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_load_nb_r_entry_changed             (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_load_nb_c_entry_changed             (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_load_nb_ext_cbtn_toggled            (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_load_nb_append_btn_clicked          (GtkButton       *button,
                                        gpointer         user_data);

void
on_load_nb_upd_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data);

void
on_load_nb_freq_entry_changed          (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_import1_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_import_sel_all_btn_clicked          (GtkButton       *button,
                                        gpointer         user_data);

void
on_import_sel_none_btn_clicked         (GtkButton       *button,
                                        gpointer         user_data);

void
on_import_sel_invert_btn_clicked       (GtkButton       *button,
                                        gpointer         user_data);

void
on_import_cancel_btn_clicked           (GtkButton       *button,
                                        gpointer         user_data);

void
on_import_exec_btn_clicked             (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_ImportWindow_delete_event           (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_ImportWindow_realize                (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_import_s2p_sel_all_btn_clicked      (GtkButton       *button,
                                        gpointer         user_data);

void
on_import_s2p_sel_none_btn_clicked     (GtkButton       *button,
                                        gpointer         user_data);

void
on_import_s2p_sel_invert_btn_clicked   (GtkButton       *button,
                                        gpointer         user_data);

void
on_import_s2p_cancel_btn_clicked       (GtkButton       *button,
                                        gpointer         user_data);

void
on_import_s2p_exec_btn_clicked         (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_Imports2pWindow_delete_event        (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_Imports2pWindow_realize             (GtkWidget       *widget,
                                        gpointer         user_data);

void
on_csv_loads1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_s2p_files1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_import_s2p_impS11_btn_clicked       (GtkButton       *button,
                                        gpointer         user_data);

void
on_import_s2p_impS22_btn_clicked       (GtkButton       *button,
                                        gpointer         user_data);

void
on_print1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_cable_cancel_btn_clicked            (GtkButton       *button,
                                        gpointer         user_data);

void
on_cable_accept_btn_clicked            (GtkButton       *button,
                                        gpointer         user_data);

void
on_cable_remove_btn_clicked            (GtkButton       *button,
                                        gpointer         user_data);

void
on_cable_add_btn_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_cable_edit_btn_clicked              (GtkButton       *button,
                                        gpointer         user_data);
