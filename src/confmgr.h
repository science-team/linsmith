/*
 *  confmgr.c: configuration manager
 *
 *  Copyright (C) 1997- John Coppens (john@jcoppens.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

enum {CFM_W_CBTN,
      CFM_W_ENTRY_STR,
      CFM_W_ENTRY_INT,
      CFM_W_ENTRY_FLOAT,
      CFM_W_ENTRY_DOUBLE,
      CFM_W_SPBTN_INT,
      CFM_W_SPBTN_FLOAT,
      CFM_W_SPBTN_DOUBLE,
      CFM_W_SPBTN_STR,
      CFM_W_RBTN,
      CFM_W_LABEL_STR,
      CFM_W_LABEL_INT,
      CFM_W_LABEL_DOUBLE,
      CFM_W_CLIST,
      CFM_W_CBOX,
      CFM_W_COMBOBOX_STR,
      CFM_W_COLORBUTTON,
      CFM_W_FONTBUTTON,
      CFM_W_LAST};

enum {CFM_T_INT,
      CFM_T_FLOAT,
      CFM_T_DOUBLE,
      CFM_T_STR,
      CFM_T_CHAR,
      CFM_T_STRLIST};

typedef struct {
  // for widget/preferences
  char typ, *wdg;
  void *data;
  int misc;
  // for save/load config routines
  char styp, *key;
  char isdef, *def;
} conf_definition;

gboolean	save_by_table(char *fn, conf_definition *tbl);
void    	load_by_table(char *fn, conf_definition *tbl);

void		parse_widget_by_table(conf_definition *tbl, GtkWidget *ref);
void		load_widget_by_table(conf_definition *tbl, GtkWidget *ref);
