enum {REMOTE_NONE, REMOTE_SOCK, REMOTE_SERIAL};

void	remote_create_fifos(void);
void	remote_send_command(char *cmd);
int	remote_receive_reply(char *reply);
void	set_remote_enabled(GtkWidget *ref, int state);
